source("IMPORT.R")

XHMM_DIR = "/projects/purces04a/fromer/xhmm/"
XHMM_EXEC = paste(XHMM_DIR, "xhmm", sep="")

OUT_DIR = "../results/phaseI/PCA"
system(paste("mkdir -p ", OUT_DIR, sep=""))

BRAIN_REGIONS = c("DLPFC")


DROP_FPKM_COLS = c("tracking_id", "class_code", "nearest_ref_id", "gene_id", "gene_short_name", "tss_id", "locus", "length")

FPKM_MATRICES = c("MERGED_ANNOTATED_ENSEMBL_GENES_CUFFLINKS_FPKM_MATRIX", "MERGED_ANNOTATED_ENSEMBL_ISOFORMS_CUFFLINKS_FPKM_MATRIX")

DATA_MATRICES = c("ENSEMBL_HTSEQ_GENES_COUNTS_MATRIX", "UCSC_HTSEQ_GENES_COUNTS_MATRIX", "MERGED_ANNOTATED_ENSEMBL_HTSEQ_GENES_COUNTS_MATRIX", "ENSEMBL_HTSEQ_DEXSEQ_EXONS_COUNTS_MATRIX", FPKM_MATRICES)



SAMPLE_META_DATA = getSampleMetaData()

ALL_RNA_SAMPLES = SAMPLE_META_DATA[, "DLPFC_RNA_isolation: Sample RNA ID"]
EXCLUDE_RNA_SAMPLES = ALL_RNA_SAMPLES[SAMPLE_META_DATA[, "DLPFC_RNA_report: Exclude?"] == 1]


for (region in BRAIN_REGIONS) {
	writeLines("")

	for (matrix in DATA_MATRICES) {
		writeLines("")

		matFile = getDataFile(matrix, region)
		if (matFile == "") {
			next
		}

		baseUseMatFile = paste(region, ".", matrix, sep="")
		useMatFile = paste(OUT_DIR, "/", baseUseMatFile, ".txt", sep="")

		writeLines(paste("Reading brain region = ", region, ", matrix = ", matrix, ": ", matFile, sep=""))

		if (matrix %in% FPKM_MATRICES) {
			mat = read.table(matFile, sep="\t", header=TRUE, row.names=1, check.names=FALSE, stringsAsFactors=FALSE, comment.char="")

			numericCols = colnames(mat)[sapply(1:ncol(mat), function(col) is.numeric(mat[, col]))]
			useCols = setdiff(numericCols, DROP_FPKM_COLS)
			mat = mat[, useCols]
		}
		else {
			mat = readNamedMatrix(matFile)
		}

		# Get rid of either: 1) samples explicitly excluded; 2) sample not in sample tracker:
		excludeSamples = union( intersect(colnames(mat), EXCLUDE_RNA_SAMPLES), setdiff(colnames(mat), ALL_RNA_SAMPLES) )
		writeLines(paste("Excluding ", length(excludeSamples), " QC- RNA-Seq samples from: ", matFile, sep=""))
		mat = mat[, setdiff(colnames(mat), excludeSamples)]

		sampleSums = apply(mat, 2, sum)
		CPMmat = 10^6 * scale(mat, center=FALSE, scale=sampleSums)
		geneMedianCPMs = apply(CPMmat, 1, median)
		keepGenes = (geneMedianCPMs >= 1)

		writeLines(paste("Retaining ONLY ", length(which(keepGenes)), " genes [of ", nrow(mat), " total] with a median CPM >= 1", sep=""))
		mat = mat[keepGenes, ]

		writeLines(paste("Transforming (transposition, log-transformation) ", matFile, sep=""))
		transformedMat = log( t(mat) + 1 )


		#writeLines(paste("Centering [gene-wise] and scaling [gene-wise] ", matFile, sep=""))
		#transformedMat = scale(transformedMat, center=TRUE, scale=TRUE)

		writeLines(paste("Centering [gene-wise] ", matFile, sep=""))
		transformedMat = scale(transformedMat, center=TRUE, scale=FALSE)


		writeLines(paste("Writing transformed ", matFile, " to ", useMatFile, sep=""))
		write.table(transformedMat, file=useMatFile, quote=FALSE, sep="\t", row.names=TRUE, col.names=NA)

		baseFilesOut = paste(OUT_DIR, "/", "PCA.", baseUseMatFile, sep="")
		runPCAcommand = paste(XHMM_EXEC, " --PCA -r ", useMatFile, " --PCAfiles ", baseFilesOut, " >& ", baseFilesOut, ".out", sep="")

		writeLines(paste("Running PCA on ", useMatFile, " [", nrow(transformedMat), " samples x ", ncol(transformedMat), " genomic intervals]", sep=""))
		writeLines(runPCAcommand)
		system(runPCAcommand)
	}
}

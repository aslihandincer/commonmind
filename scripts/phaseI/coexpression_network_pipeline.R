#
#
# The Rclusterpp and coexpp packages need to be installed from source:
# https://github.com/nolanlab/Rclusterpp
# https://bitbucket.org/multiscale/coexpp
#
#
# 1. Install Rclusterpp by running, from command line:
# git clone https://github.com/nolanlab/Rclusterpp.git
#
# cd *Rclusterpp*
# mkdir -p PATH_TO_INSTALL
# R CMD INSTALL . -l PATH_TO_INSTALL
#
#
# 2. Install dependencies by running, from R:
#
# install.packages(c("Rcpp", "RcppEigen", "WGCNA"))
#
# source("http://bioconductor.org/biocLite.R")
# biocLite("impute")
# 
# 
# 3. Run, from command-line:
# Latest source code can be downloaded from:
# https://bitbucket.org/multiscale/coexpp/get/master.zip
# or by running:
# git clone git@bitbucket.org:multiscale/coexpp.git
# 
# cd *coexpp*
# mkdir -p PATH_TO_INSTALL
# R CMD INSTALL . -l PATH_TO_INSTALL
#
#
library(coexpp)

library(ggplot2)
library(reshape2)
library(sqldf)
library(gplots)

require(synapseClient)

source('IMPORT.R')
source('findBestGeneSetMatches.R')

source('heatmap3.R')


synapseLogin()


THIS_SCRIPT_PATH = SET_THIS_SCRIPT()



coexpression_network_pipeline <- function(
    
    BRAIN_REGION = "DLPFC",
    EXPRESSION_DATA_NAME = "ENSEMBL",

    NETWORK_EXPR_INPUT_SYNAPSE_ID = "syn2713355",
    NETWORK_EXPR_INPUT_FILES_PREFIX = NULL, # "DLPFC.ensembl.KNOWN"    
    NETWORK_EXPR_MAT.SUFFIX = ".ADJUSTED.VOOM_NORMALIZED.GE.WEIGHTED_RESIDUALS.tsv",
    NETWORK_COVARS.SUFFIX = ".ALL.SAMPLE_COVARIATES.tsv",

    DIFF_EXPR_INPUT_SYNAPSE_ID = "syn2713679",
    DIFF_EXPR_INPUT_FILES_PREFIX = "DLPFC.ensembl.DxSCZ.DE.KNOWN_AND_SVA",
    DIFF_EXPR_GENES_TABLE.SUFFIX = ".ADJUSTED.VOOM_NORMALIZED.LIMMA_FIT.tsv",
    MAX_DE_FDR = 0.05,

    RESULTS_DIR = ".",

    ROOT_STORE_DIR_SYNAPSE_ID = "syn2711913",
    SYNAPSE_SUB_DIR_NAME = NULL,
    STORE_TO_SYNAPSE = FALSE,

    SAVE_WORKSPACE = FALSE,
    ALWAYS_SAVE_DATA = FALSE,

    # Optionally, can give latest date for files to retrieve, in format: "2015-1-1"
    LATEST_DATE_FOR_SYNAPSE_FILES = NULL,

    COEXPP_NUM_THREADS = 32,

    CLUSTERING_CUT_METHOD = "tree", # choices are: "tree", "hybrid"

    NUM_HIGH_VAR_GENES_TO_RETAIN = NULL,
    GENE_VARIANCE_FRACTION_TRIM_EACH_SIDE = 0.05,

    REMOVE_SAMPLE_OUTLIERS = TRUE,
    NUM_SD_SAMPLE_OUTLIERS = 4,
    MIN_NUM_SAMPLES_PER_CLUSTER = 10,

    BETA = NULL, # 6 # (hard threshold default for unsigned networks)
    R_SQUARED_CUT = 0.8, # 0.9

    TOM_PLOT.RAISE_POWER = 50,

    EXTERNAL_GENE_ANNOTATIONS_SYNAPSE_ID = "syn2749071",
    EXTERNAL_ANNOTATIONS_USE_MAPPED_GENE_IDS = TRUE,
    
    MATCH_TO_MODULES.HANDLE_GENES = "union",
    MATCH_TO_MODULES.FISHER_ALTERNATIVE = "greater",
    MATCH_TO_MODULES.P_ADJUST_ALL_ANNOTS1 = TRUE,
    MATCH_TO_MODULES.P_ADJUST_METHOD = "bonferroni",
    MATCH_TO_MODULES.MAX_P_ADJUST = 0.05,

    FACTOR_COVARIATES = c(
        "Dx"
        ),

    PRIMARY_VARS = c(
        'Dx'
        ),

    MODULE_DE.ADJUST_METHOD = "fdr",
    DE_PLOT_COL_PALETTE = colorpanel(75, "magenta", "white", "green"),
    MAX_NUM_MODULE_NAMES_PLOT = 3,

    DECIMAL_PRECISION = 3
    
    ) {



    current.warn = getOption('warn')
    # warnings are printed as they occur:
    options(warn=1)


    ######################################################################
    EXPRESSION_DATA_LIST = list(
        "DLPFC" = list(
            "UCSC"          = list(fileName="ucsc"),
            "ENSEMBL"       = list(fileName="ensembl", geneIDmapFunc=function(maxDate) {retrieveEnsemblToHGNCmap("syn2508851", maxDate)}),
            "Merged"        = list(fileName="merged"),
            "ENSEMBL Exons" = list(fileName="ensembl_exons")
            )
        )
    ######################################################################


    ################################################################################
    # Check input parameters:
    ################################################################################
    if (!(BRAIN_REGION %in% names(EXPRESSION_DATA_LIST)) || !(EXPRESSION_DATA_NAME %in% names(EXPRESSION_DATA_LIST[[BRAIN_REGION]]))) {
        stop(paste("Unable to find '", EXPRESSION_DATA_NAME, "' gene expression in the '", BRAIN_REGION, "' brain region", sep=""))
    }
    EXPRESSION_DATA_INFO = EXPRESSION_DATA_LIST[[BRAIN_REGION]][[EXPRESSION_DATA_NAME]]
    BRAIN_REGION_COEXPRESSION_NAME = paste(BRAIN_REGION, " ", EXPRESSION_DATA_NAME, " ", "coexpression", sep="")
    BRAIN_REGION_COEXPRESSION_FILE_NAME = paste(BRAIN_REGION, ".", EXPRESSION_DATA_INFO$fileName, ".", "coexpr", sep="")

    ensureDir(RESULTS_DIR)

    if (is.null(SYNAPSE_SUB_DIR_NAME)) {
        SYNAPSE_SUB_DIR_NAME = paste(BRAIN_REGION_COEXPRESSION_NAME, " - ", format(Sys.time(), "%a %b %d %Y %H_%M_%S"), sep="")
    }


    ALWAYS_SAVE_DATA = ALWAYS_SAVE_DATA & SAVE_WORKSPACE


    BASE_OUT_FILE_PREFIX = paste(RESULTS_DIR, "/", BRAIN_REGION_COEXPRESSION_FILE_NAME, sep="")

    cacheRdata_file = paste(BASE_OUT_FILE_PREFIX, ".", "coexpression_network_pipeline.RData", sep="")
    
    RUN_LOG.FILE = paste(BASE_OUT_FILE_PREFIX, '.run_log', '.txt', sep="")

    SAMPLE_CLUSTERING.FILE = paste(BASE_OUT_FILE_PREFIX, ".sample_clustering", ".pdf", sep="")
    SOFT_THRESHOLDING.FILE = paste(BASE_OUT_FILE_PREFIX, ".soft_thresholding.pdf", sep="")
    SCALE_FREE_PLOT.FILE = paste(BASE_OUT_FILE_PREFIX, ".scale_free_plot.pdf", sep="")
    GENE_CLUSTERING.FILE = paste(BASE_OUT_FILE_PREFIX, ".gene_clustering.pdf", sep="")
    TOM_CLUSTERING.FILE = paste(BASE_OUT_FILE_PREFIX, ".topological_overlap.png", sep="")
    MODULES.FILE = paste(BASE_OUT_FILE_PREFIX, ".clustering_modules.tsv", sep="")

    MODULE_EIGENGENES.FILE = paste(BASE_OUT_FILE_PREFIX, ".module_eigengenes", ".tsv", sep="")
    MODULE_EIGENGENES_PLOT.FILE = paste(BASE_OUT_FILE_PREFIX, ".module_eigengenes_heatmap", ".pdf", sep="")
    MODULE_METRICS.FILE = paste(BASE_OUT_FILE_PREFIX, ".module_metrics", ".tsv", sep="")
    MODULES_EXPRESSION_PLOT.FILE = paste(BASE_OUT_FILE_PREFIX, ".modules_expression", ".pdf", sep="")
    MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS.FILE = paste(BASE_OUT_FILE_PREFIX, ".modules_vs_differential_expression.enriched_or_depleted", ".tsv", sep="")
    MODULE_GENE_EXTERNAL_GENE_OVERLAPS.FILE = paste(BASE_OUT_FILE_PREFIX, ".modules_vs_external", ".tsv", sep="")

    MODULE_DE_HEATMAP_FILE = paste(BASE_OUT_FILE_PREFIX, '.DE.module_eigengenes', '.pdf', sep="")
    BASE_MODULE_DE_TEXT_FILE_SUFFIX = paste('.DE.module_eigengenes', '.tsv', sep="")
    
    STORE_SYNAPSE_FILES = list(
        SAMPLE_CLUSTERING.FILE,
        SOFT_THRESHOLDING.FILE, SCALE_FREE_PLOT.FILE,
        GENE_CLUSTERING.FILE, TOM_CLUSTERING.FILE, MODULES.FILE,
        MODULE_EIGENGENES.FILE, MODULE_EIGENGENES_PLOT.FILE, MODULE_METRICS.FILE, MODULES_EXPRESSION_PLOT.FILE,
        MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS.FILE, MODULE_GENE_EXTERNAL_GENE_OVERLAPS.FILE,
        MODULE_DE_HEATMAP_FILE
        )


    ALL_USED_SYNAPSE_OBJECTS = list()
    ################################################################################

    
    THIS_EXECUTED = START_SCRIPT(PARAMS, RUN_LOG.FILE, STORE_TO_SYNAPSE)


    ################################################################################
    # Process some input arguments:
    ################################################################################
    GENE_ID_MAP = NULL
    if ("geneIDmapFunc" %in% names(EXPRESSION_DATA_INFO)) {
        geneIDmapFunc = EXPRESSION_DATA_INFO[["geneIDmapFunc"]]
        GENE_ID_MAP = geneIDmapFunc(LATEST_DATE_FOR_SYNAPSE_FILES)
    }
    ################################################################################



    ################################################################################
    # Functions:
    ################################################################################


    
    ################################################################################
    #
    # LOAD DATA
    #
    ################################################################################
    NETWORK_RETRIEVE_DATA = c("NETWORK_EXPR_MAT", "NETWORK_COVARS")
    ALL_USED_SYNAPSE_OBJECTS = c(ALL_USED_SYNAPSE_OBJECTS, assignSynapseFileNames(NETWORK_EXPR_INPUT_SYNAPSE_ID, NETWORK_RETRIEVE_DATA, maxDate=LATEST_DATE_FOR_SYNAPSE_FILES, filesPrefix=NETWORK_EXPR_INPUT_FILES_PREFIX))
    
    # NETWORK_EXPR_MAT must haves genes as columns:
    NETWORK_EXPR_MAT = t(readNamedMatrix(NETWORK_EXPR_MAT.FILE))
    writeLines(paste("\nInitial expression matrix of ", ncol(NETWORK_EXPR_MAT), " genes in ", nrow(NETWORK_EXPR_MAT), " samples.", sep=""))

    NETWORK_COVARS = read.table(NETWORK_COVARS.FILE, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE, quote="")
    BRAIN_REGION_RNA_ID_COL = paste(BRAIN_REGION, '_RNA_isolation: Sample RNA ID', sep="")
    rownames(NETWORK_COVARS) = NETWORK_COVARS[, BRAIN_REGION_RNA_ID_COL]


    DIFF_EXPR_RETRIEVE_DATA = c("DIFF_EXPR_GENES_TABLE")
    ALL_USED_SYNAPSE_OBJECTS = c(ALL_USED_SYNAPSE_OBJECTS, assignSynapseFileNames(DIFF_EXPR_INPUT_SYNAPSE_ID, DIFF_EXPR_RETRIEVE_DATA, maxDate=LATEST_DATE_FOR_SYNAPSE_FILES, filesPrefix=DIFF_EXPR_INPUT_FILES_PREFIX))

    DIFF_EXPR_GENES_TABLE = read.table(DIFF_EXPR_GENES_TABLE.FILE, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE, quote="")
    rownames(DIFF_EXPR_GENES_TABLE) = DIFF_EXPR_GENES_TABLE[, "genes"]
    DIFF_EXPR_GENES = DIFF_EXPR_GENES_TABLE[DIFF_EXPR_GENES_TABLE[, "adj.P.Val"] <= MAX_DE_FDR, "genes"]

    
    EXTERNAL_GENE_ANNOTATIONS_DATA_AND_SYNAPSE = retrieveGeneAnnotations(EXTERNAL_GENE_ANNOTATIONS_SYNAPSE_ID, LATEST_DATE_FOR_SYNAPSE_FILES)
    EXTERNAL_GENE_ANNOTATIONS = EXTERNAL_GENE_ANNOTATIONS_DATA_AND_SYNAPSE$geneAnnotations
    ALL_USED_SYNAPSE_OBJECTS[[length(ALL_USED_SYNAPSE_OBJECTS)+1]] = EXTERNAL_GENE_ANNOTATIONS_DATA_AND_SYNAPSE$geneAnnotationsSynapseObject

    
    
    ################################################################################
    # Pre-filter "outlier" genes and samples:
    ################################################################################
    coexppSetThreads(COEXPP_NUM_THREADS)

    # Keep the NUM_HIGH_VAR_GENES_TO_RETAIN genes with the highest variance:
    if (!is.null(NUM_HIGH_VAR_GENES_TO_RETAIN)) {
        # Computes variance per gene after removing highest and lowest percentage from distribution:
        geneVars = apply(NETWORK_EXPR_MAT, 2, FUN=function(x, percent.trim) { var(sort(x)[floor(percent.trim * length(x)):ceiling((1-percent.trim) * length(x))]) }, percent.trim=GENE_VARIANCE_FRACTION_TRIM_EACH_SIDE)
        retainGenes = names(sort(geneVars, decreasing=TRUE))[1:NUM_HIGH_VAR_GENES_TO_RETAIN]
        NETWORK_EXPR_MAT = NETWORK_EXPR_MAT[, retainGenes]
    }

    gene.set = goodSamplesGenes(NETWORK_EXPR_MAT, verbose=3)
    if (!gene.set$allOK) {
        if (sum(!gene.set$goodGenes) > 0)
            writeLines(paste("\nRemoving genes: ", paste(colnames(NETWORK_EXPR_MAT)[!gene.set$goodGenes], collapse = ", "), sep=""))
        if (sum(!gene.set$goodSamples) > 0)
            writeLines(paste("\nRemoving samples: ", paste(rownames(NETWORK_EXPR_MAT)[!gsg$goodSamples], collapse = ", "), sep=""))
        
        # Remove the "bad" genes and samples from the data:
        NETWORK_EXPR_MAT = NETWORK_EXPR_MAT[gene.set$goodSamples, gene.set$goodGenes]
    }

    # Check for sample outliers:
    sampleTree = flashClust(dist(NETWORK_EXPR_MAT), method="average")
    # Sub-optimal since the distribution of tree heights is clearly asymmetric and is right-tailed:
    sampleHeightThreshold = mean(sampleTree$height) + NUM_SD_SAMPLE_OUTLIERS * sd(sampleTree$height)
    if (REMOVE_SAMPLE_OUTLIERS) {
        sampleCut = cutreeStatic(sampleTree, cutHeight=sampleHeightThreshold, minSize=MIN_NUM_SAMPLES_PER_CLUSTER)
        removeSamples = rownames(NETWORK_EXPR_MAT)[sampleCut == 0]
        writeLines(paste("\nRemoving ", length(removeSamples), " outlier samples at tree cut at height > ", sprintf("%.2f", sampleHeightThreshold), " and requiring >= ", MIN_NUM_SAMPLES_PER_CLUSTER, " samples per cluster: ", paste(removeSamples, collapse=", "), sep=""))
        NETWORK_EXPR_MAT = NETWORK_EXPR_MAT[setdiff(rownames(NETWORK_EXPR_MAT), removeSamples), ]
    }

    pdf(SAMPLE_CLUSTERING.FILE, width=48, height=10)
    par(cex=0.6)
    par(mar=c(0,4,2,0))
    title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "sample clustering to detect outliers", sep="")
    plot(sampleTree, main=title, sub="", xlab="", cex.lab=1.5, cex.axis=1.5, cex.main=2)
    abline(h=sampleHeightThreshold, col = "red") # exclude samples in "small" clusters above this line
    dev.off()

    # Ensure consistency between expression data and sample covariates:
    ANALYZE_SAMPLES = rownames(NETWORK_EXPR_MAT)
    ANALYZE_SAMPLES = ANALYZE_SAMPLES[ANALYZE_SAMPLES %in% rownames(NETWORK_COVARS)]
    NETWORK_EXPR_MAT = NETWORK_EXPR_MAT[ANALYZE_SAMPLES, ]
    NETWORK_COVARS = NETWORK_COVARS[ANALYZE_SAMPLES, ]

    
    ################################################################################
    # Run WGCNA using coexpp:
    ################################################################################
    writeLines(paste("\nStarting ", BRAIN_REGION_COEXPRESSION_NAME, " analysis on ", ncol(NETWORK_EXPR_MAT), " genes in ", nrow(NETWORK_EXPR_MAT), " samples.", sep=""))
    wgcnaCoexNet = coexpressionAnalysis(NETWORK_EXPR_MAT, beta=BETA, RsquaredCut=R_SQUARED_CUT, cut=CLUSTERING_CUT_METHOD)
    saveData(saveFile=cacheRdata_file, saveNow=ALWAYS_SAVE_DATA)

    chosenBeta = wgcnaCoexNet$clusters@beta
    if (chosenBeta == 1) {
        stop("ERROR: BETA IS 1")
    }
    writeLines(paste("\nBETA IS: ", chosenBeta, sep=""))

    # Soft threshold plot:
    if (is.null(BETA)) {
        pdf(SOFT_THRESHOLDING.FILE)
        cex1 = 0.9
        cex2 = 1.2
        
        # Scale-free topology fit index as a function of the soft-thresholding power:
        softThreshStats = wgcnaCoexNet$clusters@sftStatistics
        softThresholds = softThreshStats[, "Power"]
        signedRsquaredScaleFreeTopo = - sign(softThreshStats[, "slope"]) * softThreshStats[, "SFT.R.sq"]
        title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "Scale independence", sep="")
        plot(softThresholds, signedRsquaredScaleFreeTopo, xlab="Soft Threshold (power)", ylab="Scale Free Topology Model Fit, signed R^2", type="n", main=title)
        text(softThresholds, signedRsquaredScaleFreeTopo, labels=softThresholds, cex=cex1, col="red")        
        abline(h=R_SQUARED_CUT, col="red", lty="dotted")
        abline(v=chosenBeta, col="red", lty="dotted")

        meanConnectivity = softThreshStats[, "mean.k."]
        title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "Mean connectivity", sep="")
        plot(softThresholds, meanConnectivity, xlab="Soft Threshold (power)", ylab="Mean Connectivity", type="n", main=title, log="y")
        text(softThresholds, meanConnectivity, labels=softThresholds, cex=cex1, col="red")
        meanConnAtBeta = softThreshStats[softThresholds == chosenBeta, "mean.k."]
        abline(h=meanConnAtBeta, col="red", lty="dotted")
        text(sort(softThresholds)[2], meanConnAtBeta, pos=3, labels=paste("|k|=", sprintf("%.1f", meanConnAtBeta), sep=""), cex=cex2, col="darkgray")
        abline(v=chosenBeta, col="red", lty="dotted")
        
        dev.off()
    }

    # Scale-free plot for selected beta:
    pdf(SCALE_FREE_PLOT.FILE)
    plotScaleFree(wgcnaCoexNet$clusters)
    dev.off()

    # Clustering plot for selected beta:
    pdf(GENE_CLUSTERING.FILE, width=15, height=5)
    title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "Gene clustering", sep="")
    plotClustering(wgcnaCoexNet$clusters, wgcnaCoexNet$geneModules, dendroLabels=FALSE, groupLabels=c(""), axes=FALSE, addGuide=FALSE, autoColorHeight=FALSE, colorHeight=0.5, ann=FALSE, main=title)
    dev.off()
    
    # Topological overlap matrix heatmap for selected beta:
    png(TOM_CLUSTERING.FILE, width=12, height=12, units="in", res=300)
    title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "topological overlap matrix", sep="")
    plotTOMHeatmap(wgcnaCoexNet$clusters, wgcnaCoexNet$geneModules, samplingThreshold=1000, plot.raise_power=TOM_PLOT.RAISE_POWER, main=title)
    dev.off()

    # Output modules for the selected beta:
    GENES_TO_MODULES = data.frame(colnames(NETWORK_EXPR_MAT), wgcnaCoexNet$geneModules, wgcnaCoexNet$intraModularStatistics)
    colnames(GENES_TO_MODULES) = c("Gene", "Module", colnames(wgcnaCoexNet$intraModularStatistics))
    rownames(GENES_TO_MODULES) = GENES_TO_MODULES[, "Gene"]
    GENES_TO_MODULES = insertMappedColumn(GENES_TO_MODULES, colNameToMap="Gene", GENE_ID_MAP)
    write.table(GENES_TO_MODULES, file=MODULES.FILE, sep="\t", quote=FALSE, row.names=FALSE, col.names=TRUE)

    # Get modules sorted by size:
    MODULE_MEMBERS = modulesDataToGeneAnnotations(GENES_TO_MODULES, geneCol="Gene")$annotToGenes
    MODULE_SIZES = sort(sapply(MODULE_MEMBERS, length), decreasing=TRUE)
    MODULE_NAMES = names(MODULE_SIZES)
    MODULE_COLORS = setNames(MODULE_NAMES, MODULE_NAMES)
    
    # Calculate module eigengenes:
    MEs0 = moduleEigengenes(NETWORK_EXPR_MAT, wgcnaCoexNet$geneModules)$eigengenes
    MEs = orderMEs(MEs0)
    rownames(MEs) = rownames(NETWORK_EXPR_MAT)
    ME_MODULES = sub(paste("^", moduleColor.getMEprefix(), sep=""), "", colnames(MEs))
    colnames(MEs) = ME_MODULES
    writeLines("Module", con=MODULE_EIGENGENES.FILE, sep="")
    write.table(formatMatrixPrecision(t(MEs)[MODULE_NAMES, ], DECIMAL_PRECISION), append=TRUE, file=MODULE_EIGENGENES.FILE, sep="\t", quote=FALSE, row.names=TRUE, col.names=NA)

    # Plot module eigengenes:
    pdf(MODULE_EIGENGENES_PLOT.FILE, width=48, height=10)
    title = paste(BRAIN_REGION_COEXPRESSION_NAME, ": ", "Module eigengenes x samples", sep="")
    #heatmap.3(MEs, Rowv=NA, Colv=NA, dendrogram="none", scale="none", revC=FALSE, density.info="none", trace="none", xlab="", main=title, symbreaks=FALSE, keysize=1.0)
    heatmap.3(MEs, dendrogram="both", scale="none", revC=FALSE, density.info="none", trace="none", xlab="", main=title, symbreaks=FALSE, keysize=1.0)
    dev.off()

    
    # Correlate modules with selected covariates:
    primary.DESIGN_AND_NETWORK_COVARS = getDesignMatrix(NETWORK_COVARS[, PRIMARY_VARS, drop=FALSE], FACTOR_COVARIATES, RELEVELS=DEFAULT_RELEVELS)
    primaryDesignMat = primary.DESIGN_AND_NETWORK_COVARS$design

    PRIMARY_COVARIATES_MAP = getCovariatesMap(primary.DESIGN_AND_NETWORK_COVARS)
    PRIMARY_VARS_INCLUDING_LEVELS = as.character(unlist(PRIMARY_COVARIATES_MAP[PRIMARY_VARS]))
    PRIMARY_PHENOTYPES = primaryDesignMat[, PRIMARY_VARS_INCLUDING_LEVELS]
    PRIMARY_PHENO_NAMES = colnames(PRIMARY_PHENOTYPES)
    
    
    # Calculate and output module-level metrics:
    MODULE_METRICS = matrix(NA, nrow=length(MODULE_NAMES), ncol=0, dimnames=list(MODULE_NAMES, c()))
    MODULE_METRICS = cbind(MODULE_METRICS, "Size"=MODULE_SIZES)

    MODULE_MEAN_EXPR = sapply(MODULE_NAMES, function(module) {moduleGenes = MODULE_MEMBERS[[module]]; return(mean(NETWORK_EXPR_MAT[, moduleGenes]))})
    MODULE_AVE_EXPR = matrix(MODULE_MEAN_EXPR, ncol=1, dimnames=list(names(MODULE_MEAN_EXPR), "AveExpr"))

    MODULE_MEAN_ME_EXPR = apply(MEs, 2, mean)
    names(MODULE_MEAN_ME_EXPR) = ME_MODULES
    MODULE_MEAN_ME_EXPR = MODULE_MEAN_ME_EXPR[MODULE_NAMES]
    MODULE_AVE_ME = matrix(MODULE_MEAN_ME_EXPR, ncol=1, dimnames=list(names(MODULE_MEAN_ME_EXPR), "AveME"))

    # Can only tabulate discrete primary variables in code below:
    TABULATE_PRIMARY_VARS = PRIMARY_VARS[PRIMARY_VARS %in% names(primary.DESIGN_AND_NETWORK_COVARS$factorsLevels)]
    for (primaryVar in TABULATE_PRIMARY_VARS) {
        varLevels = levels(primary.DESIGN_AND_NETWORK_COVARS$covariatesDataFrame[[primaryVar]])

        for (varLevel in varLevels) {
            assessSamps = ANALYZE_SAMPLES[ NETWORK_COVARS[ANALYZE_SAMPLES, primaryVar] == varLevel ]

            moduleMeanExpr = sapply(MODULE_NAMES, function(module) {moduleGenes = MODULE_MEMBERS[[module]]; return(mean(NETWORK_EXPR_MAT[assessSamps, moduleGenes]))})
            MODULE_AVE_EXPR = cbind(MODULE_AVE_EXPR, matrix(moduleMeanExpr, ncol=1, dimnames=list(names(moduleMeanExpr), paste("AveExpr", ":", primaryVar, ":", varLevel, sep=""))))

            moduleMeanMEexpr = apply(MEs[assessSamps, ], 2, mean)
            names(moduleMeanMEexpr) = ME_MODULES
            moduleMeanMEexpr = moduleMeanMEexpr[MODULE_NAMES]
            MODULE_AVE_ME = cbind(MODULE_AVE_ME, matrix(moduleMeanMEexpr, ncol=1, dimnames=list(names(moduleMeanMEexpr), paste("AveME", ":", primaryVar, ":", varLevel, sep=""))))
        }
    }

    MODULE_METRICS = cbind(MODULE_METRICS, MODULE_AVE_EXPR, MODULE_AVE_ME)
    
    writeLines("Module", con=MODULE_METRICS.FILE, sep="")
    metricsMat = cbind(MODULE_METRICS[, "Size", drop=FALSE], formatMatrixPrecision(MODULE_METRICS[, setdiff(colnames(MODULE_METRICS), "Size"), drop=FALSE], DECIMAL_PRECISION))
    write.table(metricsMat, append=TRUE, file=MODULE_METRICS.FILE, sep="\t", quote=FALSE, row.names=TRUE, col.names=NA)

    DE_GENES_STRING = paste("Diff Expr [FDR <= ", MAX_DE_FDR, "]", sep="")
    DE_GENES_DATA_FRAME = data.frame(rownames(GENES_TO_MODULES) %in% DIFF_EXPR_GENES, row.names=rownames(GENES_TO_MODULES))
    colnames(DE_GENES_DATA_FRAME) = DE_GENES_STRING

    # Plot module-wise gene expression levels:
    pdf(MODULES_EXPRESSION_PLOT.FILE, height=9, width=18)
    for (primaryVar in TABULATE_PRIMARY_VARS) {
        EXPR_VAR = "Expression level"
        
        #GENE_BY_EXPR_AND_MODULES = data.frame(t(NETWORK_EXPR_MAT), GENES_TO_MODULES[, c("Gene", "Module")], check.rows=FALSE, check.names=FALSE, stringsAsFactors=FALSE)
        #GENE_MODULE_EXPRESSION = melt(GENE_BY_EXPR_AND_MODULES, id=c("Gene", "Module"), value.name=EXPR_VAR)

        #GENE_MODULE_EXPRESSION = data.frame(apply(NETWORK_EXPR_MAT, 2, median), GENES_TO_MODULES[, c("Gene", "Module")], check.rows=FALSE, check.names=FALSE, stringsAsFactors=FALSE)
        #colnames(GENE_MODULE_EXPRESSION)[1] = EXPR_VAR
        
        GENE_MODULE_EXPRESSION = data.frame(t(sapply(colnames(NETWORK_EXPR_MAT), function(gene) {tapply(NETWORK_EXPR_MAT[, gene], primary.DESIGN_AND_NETWORK_COVARS$covariatesDataFrame[[primaryVar]], median)})), GENES_TO_MODULES[, c("Gene", "Module")], check.rows=FALSE, check.names=FALSE, stringsAsFactors=FALSE)
        GENE_MODULE_EXPRESSION = cbind(GENE_MODULE_EXPRESSION, DE_GENES_DATA_FRAME)
        GENE_MODULE_EXPRESSION = melt(GENE_MODULE_EXPRESSION, id=c("Gene", "Module", DE_GENES_STRING), variable.name=primaryVar, value.name=EXPR_VAR)

        DF_MODULE_SIZES = data.frame("Module"=rownames(MODULE_METRICS), "Size"=MODULE_METRICS[, "Size"], check.rows=FALSE, check.names=FALSE, stringsAsFactors=FALSE)
        ORDERED_GENES = as.character(sqldf("SELECT Gene FROM GENES_TO_MODULES g, DF_MODULE_SIZES m WHERE g.Module == m.Module ORDER BY m.Size, g.k_in DESC")[, "Gene"])

        GENE_MODULE_EXPRESSION[, "Gene"] = factor(GENE_MODULE_EXPRESSION[, "Gene"], levels=rev(ORDERED_GENES))
        GENE_MODULE_EXPRESSION[, "Module"] = factor(GENE_MODULE_EXPRESSION[, "Module"], levels=MODULE_NAMES)

        AES = aes_string(x="Gene", y=getSymbolicNamesVector(EXPR_VAR), fill="Module", shape=getSymbolicNamesVector(primaryVar), color=getSymbolicNamesVector(DE_GENES_STRING), alpha=getSymbolicNamesVector(DE_GENES_STRING), size=getSymbolicNamesVector(DE_GENES_STRING))
        useGuideLegend = guide_legend(override.aes=list(size=5, shape=21))

        mPlot = ggplot(GENE_MODULE_EXPRESSION)
        #mPlot = mPlot + geom_boxplot(AES, outlier.colour="transparent")
        mPlot = mPlot + geom_point(AES)
        mPlot = mPlot + scale_fill_manual(values=MODULE_COLORS) + scale_shape_manual(values=21:25) + scale_color_manual(values=c(NA, "black")) + scale_alpha_manual(values=c(0.25, 1)) + scale_size_manual(values=c(2, 4))
        mPlot = mPlot + guides(fill=useGuideLegend, color=useGuideLegend) + theme(axis.ticks.x=element_blank(), axis.text.x=element_blank(), panel.background=element_rect(fill="white", color=NA), panel.border=element_rect(fill=NA, color="black"))
        
        print(mPlot)
    }    
    dev.off()

    
    # Calculate overlaps between modules and differential expression:
    MODULE_GENE_ANNOTATIONS = modulesDataToGeneAnnotations(GENES_TO_MODULES, geneCol="Gene")
    
    DIFFERENTIAL_GENE_ANNOTATIONS = convertTableToGeneAnnotations(cbind("Gene"=DIFF_EXPR_GENES, "Set"=DE_GENES_STRING), "Gene", "Set")
    MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS = findBestGeneSetMatches(MODULE_GENE_ANNOTATIONS, DIFFERENTIAL_GENE_ANNOTATIONS, HANDLE_GENES="union", FISHER_ALTERNATIVE="two.sided", P_ADJUST_ALL_ANNOTS1=TRUE, P_ADJUST_METHOD="bonferroni", MAX_P_ADJUST=1)
    MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS = MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[sort(MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[, "adjP"], decreasing=FALSE, index.return=TRUE)$ix, ]
    write.table(formatMatrixPrecision(MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS, DECIMAL_PRECISION), file=MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS.FILE, sep="\t", quote=FALSE, row.names=FALSE, col.names=TRUE)

    
    # Calculate overlaps between modules and external gene annotations:
    geneCol = "Gene"
    if (EXTERNAL_ANNOTATIONS_USE_MAPPED_GENE_IDS) {
        geneCol = "MAPPED_Gene"
    }
    FOR_EXTERNAL.MODULE_GENE_ANNOTATIONS = modulesDataToGeneAnnotations(GENES_TO_MODULES, geneCol=geneCol)
    
    MODULE_GENE_EXTERNAL_GENE_OVERLAPS = findBestGeneSetMatches(FOR_EXTERNAL.MODULE_GENE_ANNOTATIONS, EXTERNAL_GENE_ANNOTATIONS, HANDLE_GENES=MATCH_TO_MODULES.HANDLE_GENES, FISHER_ALTERNATIVE=MATCH_TO_MODULES.FISHER_ALTERNATIVE, P_ADJUST_ALL_ANNOTS1=MATCH_TO_MODULES.P_ADJUST_ALL_ANNOTS1, P_ADJUST_METHOD=MATCH_TO_MODULES.P_ADJUST_METHOD, MAX_P_ADJUST=MATCH_TO_MODULES.MAX_P_ADJUST)
    write.table(formatMatrixPrecision(MODULE_GENE_EXTERNAL_GENE_OVERLAPS, DECIMAL_PRECISION), file=MODULE_GENE_EXTERNAL_GENE_OVERLAPS.FILE, sep="\t", quote=FALSE, row.names=FALSE, col.names=TRUE)


    # Correlate module eigengenes to phenotypes:
    #
    # modulePhenoCorME = cor(MEs, PRIMARY_PHENOTYPES, use="pairwise.complete.obs", method="spearman") #pairwise spearman correlation
    modulePhenoCor = cor(MEs[, MODULE_NAMES], PRIMARY_PHENOTYPES, use="pairwise.complete.obs")
    modulePhenoPvalue = corPvalueStudent(modulePhenoCor, nrow(MEs))
    modulePhenoPvalueAdj = apply(modulePhenoPvalue, 2, function(p) {p.adjust(p, method=MODULE_DE.ADJUST_METHOD)})

    pdf(MODULE_DE_HEATMAP_FILE, height=16, width=18)

    cellnote = sapply(colnames(modulePhenoCor), function(col) paste("r=", sprintf("%.2f", modulePhenoCor[, col]), "; p=", sprintf("%.2g", modulePhenoPvalue[, col]), " (", sprintf("%.2g", modulePhenoPvalueAdj[, col]), ")", sep=""))

    notecol = sapply(colnames(modulePhenoPvalueAdj), function(col) ifelse(modulePhenoPvalueAdj[, col] <= MAX_DE_FDR, "black", "gray"))

    # bottom, right:
    mar_margins = c(4, 1)

    # bottom, left, top, right:
    par(xpd=TRUE)
    par(oma=c(5, 2, 1, 1))

    moduleHeatMapData = modulePhenoCor
    moduleHeatMapData = cbind(moduleHeatMapData, "Name"=0, "Diff Expr"=0)

    SORTED.MODULE_GENE_EXTERNAL_GENE_OVERLAPS = MODULE_GENE_EXTERNAL_GENE_OVERLAPS[sort(MODULE_GENE_EXTERNAL_GENE_OVERLAPS[, "jaccard"], decreasing=TRUE, index.return=TRUE)$ix, ]
    noteModuleNames = sapply(MODULE_NAMES, function(module) {names=SORTED.MODULE_GENE_EXTERNAL_GENE_OVERLAPS[SORTED.MODULE_GENE_EXTERNAL_GENE_OVERLAPS[, "annot1"] == module, "annot2"]; if (length(names)==0) {return(".")}; chooseNames=1:min(MAX_NUM_MODULE_NAMES_PLOT, length(names)); return(paste(names[chooseNames], collapse="\n"))})
    cellnote = cbind(cellnote, noteModuleNames)

    noteModuleDE = sapply(MODULE_NAMES, function(module) {ind = which(MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[, "annot1"] == module); if (MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[ind, "adjP"] > MAX_DE_FDR) {return(".")}; OR=MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[ind, "OR"]; N=MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[ind, "intersect"]; p=MODULE_GENE_DIFFERENTIAL_GENE_OVERLAPS[ind, "p"]; stats=paste("(N=", N, "; OR=", sprintf("%.2g", OR), "; p=", sprintf("%.2g", p), ")", sep=""); if (OR > 1) {return(paste("Enriched for DE genes\n", stats, sep=""))} else {return(paste("Depleted for DE genes\n", stats, sep=""))}})
    cellnote = cbind(cellnote, noteModuleDE)

    notecol = cbind(notecol, "black", "black")

    labRow = paste(names(MODULE_SIZES), " (", MODULE_SIZES, ")", sep="")

    heatmap.3(moduleHeatMapData, col=DE_PLOT_COL_PALETTE, RowSideColors=matrix(MODULE_COLORS, dimnames=list(NULL, "")), Rowv=FALSE, Colv=FALSE, dendrogram="none", scale="none", revC=FALSE, density.info="none", trace="none", xlab="", labRow=labRow, labRow.side=2, labRow.line=8, maxScaledVal=1, requireMaxScaledVal=TRUE, symbreaks=TRUE, keysize=1, KeyValueName="", main="", margins=mar_margins, cexRow=(1 + 1/log10(nrow(modulePhenoCor))), cexCol=2, cellnote=cellnote, notecol=notecol, sepcolor="black", rowsep=1:nrow(moduleHeatMapData))

    dev.off()
    

    ME_MODULE_SIZES = MODULE_SIZES[MODULE_NAMES]
    for (pheno in PRIMARY_PHENO_NAMES) {
        p = modulePhenoPvalue[, pheno]
        adjP = modulePhenoPvalueAdj[, pheno]
        modulePhenoTable = data.frame(size=ME_MODULE_SIZES, cor=modulePhenoCor[, pheno], p=p, adjP=adjP)

        sortInds = sort(adjP, decreasing=FALSE, index.return=TRUE)$ix
        modulePhenoTable = modulePhenoTable[sortInds, ]

        PRIMARY_ME.FILE = paste(BASE_OUT_FILE_PREFIX, ".", pheno, BASE_MODULE_DE_TEXT_FILE_SUFFIX, sep="")
        STORE_SYNAPSE_FILES[[length(STORE_SYNAPSE_FILES)+1]] = PRIMARY_ME.FILE
        
        writeLines("Module", con=PRIMARY_ME.FILE, sep="")
        write.table(modulePhenoTable, append=TRUE, file=PRIMARY_ME.FILE, sep="\t", quote=FALSE, row.names=TRUE, col.names=NA)
    }



    
    # Force a save of all data at the end:
    saveData(saveFile=cacheRdata_file, saveNow=SAVE_WORKSPACE)

    SYNAPSE_SUB_DIR_ID = END_SCRIPT(RUN_LOG.FILE, THIS_EXECUTED, ALL_USED_SYNAPSE_OBJECTS, STORE_SYNAPSE_FILES, ROOT_STORE_DIR_SYNAPSE_ID, SYNAPSE_SUB_DIR_NAME, STORE_TO_SYNAPSE, ACTIVITY_NAME='Run WGCNA coexpression network analysis pipeline')$SYNAPSE_SUB_DIR_ID

    options(warn=current.warn)

    invisible(SYNAPSE_SUB_DIR_ID)
}

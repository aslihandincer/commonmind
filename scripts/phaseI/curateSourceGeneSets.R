#########################################################
#### Curate source gene sets for enrichment analysis ####
#########################################################
# File dependencies
# Downalod following files and store in working directory
# Pathway Commons 2: http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.6.All.GSEA.gmt.gz
# Biocarta: http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c2.cp.biocarta.v4.0.symbols.gmt
# KEGG: http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c2.cp.kegg.v4.0.symbols.gmt
# TRANSFAC: http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c3.tft.v4.0.symbols.gmt
# Targetscan: http://www.targetscan.org/vert_61/vert_61_data_download/Summary_Counts.txt.zip
#########################################################
# Clear Screen
cat("\014")  

# Clear workspace
rm(list=ls())

# Load external libraries
library('synapseClient')
library('AnnotationDbi')
library('plyr')
library('data.table')
library('GSA')

# Bioconductor annotation databases
library('org.Hs.eg.db')
library('GO.db')
library('reactome.db')
library('KEGG.db')
library('PANTHER.db')

# Login to synapse
synapseLogin()
##################################################################################################################################


##################################################################################################################################
#### Input Parameters ####
# setwd('~/Work/CMC_Analysis/Resources/')

SYNAPSE_STORE = T;
parentId = 'syn3168452';
executed = 'https://bitbucket.org/commonmind/commonmind/src/61f888515595926147f669414b64782f7a8d96f0/scripts/phaseI/curatePrimaryGeneSets.R';

sourceFolderName = "GeneSets_Source"
##################################################################################################################################

##################################################################################################################################
##############################################################
#### Download gene sets from sources and store in synapse ####
##############################################################
# Create local directory
system(paste('mkdir ./"',sourceFolderName,'"',sep=''))

# Create synapse directory (if exist obtain directory info
if (SYNAPSE_STORE){
  # Create folder in synapse
  SOURCE_FOLDER <- Folder(name = sourceFolderName, parentId = parentId)
  annotations(SOURCE_FOLDER) <- list(Curated = 'NO',Organism = 'Human', IdType = 'Symbol', Description = 'Genesets from sources')
  SOURCE_FOLDER <- synStore(SOURCE_FOLDER)
}

############
#### GO ####
############
# Get GO mappings from org.Hs.eg.db (http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz)
GO <- select(org.Hs.eg.db, keys = keys(org.Hs.eg.db, keytype="GO"), columns="SYMBOL", keytype="GO")
# TAS: traceable author statement
# NAS: non-traceable author statement
# ND: no biological data available
# IC: inferred by curator
# Remove IC, ND, NAS, TAS
GO <- GO[(GO$EVIDENCE != 'IC' & GO$EVIDENCE != 'ND' & GO$EVIDENCE != 'NAS' & GO$EVIDENCE != 'TAS'),]

# Convert data frame to data table
GO <- data.table(GO)

# Combine all the genes together to form unique sets
GO <- GO[, list(SYMBOL = paste(unique(SYMBOL),collapse='|')), by = GO]

# Annotate GO terms (http://www.bioconductor.org/packages/release/data/annotation/src/contrib/GO.db_3.0.0.tar.gz)
GOTERM <- data.table(as.data.frame(GOTERM))
GOTERM <- GOTERM[,!duplicated(colnames(GOTERM)),with=F]
GOTERM <- GOTERM[,!(colnames(GOTERM) == 'Synonym' | colnames(GOTERM) == 'Secondary'),with=F]
GOTERM <- unique(GOTERM)

# Merge GO and GO annotations 
setkey(GOTERM,'go_id')
setnames(GO,c('GO','SYMBOL'),c('go_id','symBeforeOverlap'))
setkey(GO,'go_id')
GO <- merge(GO,GOTERM,by='go_id')

# Add columns for Datasource, IdType, Organism
setnames(GO,c('go_id','Term'),c('Source Id','Name'))
GO <- GO[,list(Name = paste('go',paste(Ontology,Name,sep='|'),sep=':'),
               Datasource = 'org.Hs.eg.db', IdType = 'symbol', Organism = 'human'),
         by=c('Source Id','symBeforeOverlap')]
setcolorder(GO,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'))          

# Save source list in local folder and in synapse
write.table(GO, file = paste('./',sourceFolderName,'/GO_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  GO_OBJ <- File(paste('./',sourceFolderName,'/GO_source.tsv',sep=''), name = 'GO',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(GO_OBJ) <- list(DataSource = 'org.Hs.eg.db',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                              Description = 'GO gene sets from Bioconductor org.Hs.eg.db database',Number_of_genesets = dim(GO)[1])
  GO_OBJ <- synStore(GO_OBJ, used = c('http://www.bioconductor.org/packages/release/data/annotation/src/contrib/GO.db_3.0.0.tar.gz',
                                      'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                     activityName='Genesets Curation', executed = executed, activityDescription='GO gene sets curation')
} 

##################
#### REACTOME ####
##################
# Get REACTOME mappings from reactome.db (http://bioconductor.org/packages/3.0/data/annotation/html/reactome.db.html)
ALIAS2EG <- as.data.frame(org.Hs.egALIAS2EG)

DBID2EG <- as.data.frame(reactomeEXTID2PATHID)
PATHNAME2DBID <- as.data.frame(reactomePATHNAME2ID)

# Merge annotation data
REACTOME <- merge(PATHNAME2DBID,merge(ALIAS2EG,DBID2EG,by='gene_id',all=T),by='DB_ID',all=T)

# Convert data frame to data table
REACTOME <- data.table(REACTOME)

# Combine all the genes together to form unique sets
REACTOME <- REACTOME[, list(path_name = paste('reactome',strsplit(path_name,': ')[[1]][2],sep=':'),
                            gene_id = paste(unique(gene_id),collapse='|'),
                            alias_symbol = paste(unique(alias_symbol),collapse='|'),
                            Datasource = 'reactome.db', IdType = 'symbol', Organism = 'human'), by = 'DB_ID']
REACTOME = REACTOME[,][path_name != 'reactome:NA']

setnames(REACTOME,c('path_name','gene_id','alias_symbol','DB_ID'),c('Name','Entrez Id','symBeforeOverlap','Source Id'))
setcolorder(REACTOME,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism','Entrez Id'))          

# Save source list in local folder and in synapse
write.table(REACTOME, file = paste('./',sourceFolderName,'/REACTOME_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  REACTOME_OBJ <- File(paste('./',sourceFolderName,'/REACTOME_source.tsv',sep=''), 
                       name = 'REACTOME',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(REACTOME_OBJ) <- list(DataSource = 'reactome.db',Curated = 'NO',Organism = 'Human', IdType = 'Symbol',
                                    Description = 'Reactome gene sets from Bioconductor reactome.db database',
                                    Number_of_genesets = dim(REACTOME)[1])
  REACTOME_OBJ <- synStore(REACTOME_OBJ, used = c('http://bioconductor.org/packages/3.0/data/annotation/src/contrib/reactome.db_1.50.0.tar.gz',
                                                  'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                           activityName='Genesets Curation', executed = executed, activityDescription='Reactome gene sets curation')
}

##############
#### KEGG ####
##############
# Get KEGG mappings from kegg.db (http://bioconductor.org/packages/3.0/data/annotation/html/KEGG.db.html)
ALIAS2EG <- as.data.frame(org.Hs.egALIAS2EG)

DBID2EG <- as.data.frame(KEGGPATHID2EXTID)
tmp <- apply(DBID2EG[,'pathway_id',drop=F],1,function(x){tmp = unlist(strsplit(x,'')); 
                                                         c(paste(tmp[1:3],collapse=""),
                                                           paste(tmp[4:length(tmp)],collapse=""))})
DBID2EG <- cbind(DBID2EG,as.data.frame(t(tmp)))
colnames(DBID2EG) <- c('pathway_id','gene_id','organism','path_id')

PATHNAME2DBID <- as.data.frame(KEGGPATHNAME2ID)

# Merge annotation data
KEGG <- merge(PATHNAME2DBID,merge(ALIAS2EG,DBID2EG,by='gene_id',all=T),by='path_id',all=T)

# Convert data frame to data table
KEGG <- data.table(KEGG)

# Combine all the genes together to form unique sets
KEGG <- KEGG[, list(path_id = paste(unique(path_id),collapse='|'),
                    pathway_id = paste(unique(pathway_id),collapse='|'),
                    gene_id = paste(unique(gene_id),collapse='|'),
                    alias_symbol = paste(unique(alias_symbol),collapse='|'),                    
                    Datasource = 'KEGG.db', IdType = 'symbol'), by = c('path_name','organism')]

KEGG <- KEGG[,list(path_name = paste('kegg',path_name,sep=':')),by=setdiff(colnames(KEGG),'path_name')][organism == 'hsa']

setnames(KEGG,c('path_name','gene_id','alias_symbol','pathway_id','organism'),c('Name','Entrez Id','symBeforeOverlap','Source Id','Organism'))
setcolorder(KEGG,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism','Entrez Id','path_id'))          

# Save source list in local folder and in synapse
write.table(KEGG, file = paste('./',sourceFolderName,'/KEGG_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  KEGG_OBJ <- File(paste('./',sourceFolderName,'/KEGG_source.tsv',sep=''), 
                       name = 'KEGG',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(KEGG_OBJ) <- list(DataSource = 'KEGG.db',Curated = 'NO',Organism = 'Human', IdType = 'Symbol',
                                Description = 'Kegg gene sets from Bioconductor kegg.db database',
                                Number_of_genesets = dim(KEGG)[1])
  KEGG_OBJ <- synStore(KEGG_OBJ, used = c('http://bioconductor.org/packages/3.0/data/annotation/src/contrib/KEGG.db_3.0.0.tar.gz',
                                          'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                           activityName='Genesets Curation', executed = executed, activityDescription='Kegg gene sets curation')
}

#################
#### PANTHER ####
#################
# Get PANTHER mappings from PANTHER.db (http://bioconductor.org/packages/3.0/data/annotation/html/PANTHER.db.html)
ALIAS2EG <- as.data.frame(org.Hs.egALIAS2EG)

# Get Panther annotations for entrez identifiers
PANTHER <- select(PANTHER.db,ALIAS2EG$gene_id,c("PATHWAY_ID","PATHWAY_TERM","SPECIES"),'ENTREZ')
PANTHER <- PANTHER[PANTHER$SPECIES == "HUMAN",]
colnames(PANTHER)[1] <- 'gene_id'

# Merge annotation data
PANTHER <- merge(PANTHER,ALIAS2EG,by='gene_id',all=T)

# Convert data frame to data table
PANTHER <- data.table(PANTHER)

# Combine all the genes together to form unique sets
PANTHER <- PANTHER[, list(PATHWAY_ID = paste(unique(PATHWAY_ID),collapse='|'),
                          gene_id = paste(unique(gene_id),collapse='|'),
                          alias_symbol = paste(unique(alias_symbol),collapse='|'),                    
                          Datasource = 'PANTHER.db', IdType = 'symbol',Organism = 'human'), by = 'PATHWAY_TERM']
PANTHER <- PANTHER[!is.na(PATHWAY_TERM),]
PANTHER <- PANTHER[,list(PATHWAY_TERM = paste('panther',PATHWAY_TERM,sep=':')),
                   by=setdiff(colnames(PANTHER),'PATHWAY_TERM')]

setnames(PANTHER,c('PATHWAY_ID','gene_id','alias_symbol','PATHWAY_TERM'),c('Source Id','Entrez Id','symBeforeOverlap','Name'))
setcolorder(PANTHER,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism','Entrez Id'))          

# Save source list in local folder and in synapse
write.table(PANTHER, file = paste('./',sourceFolderName,'/PANTHER_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  PANTHER_OBJ <- File(paste('./',sourceFolderName,'/PANTHER_source.tsv',sep=''),
                      name = 'PANTHER',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(PANTHER_OBJ) <- list(DataSource = 'PANTHER.db',Curated = 'NO',Organism = 'Human', IdType = 'Symbol',
                                   Description = 'PANTHER gene sets from Bioconductor PANTHER.db database',
                                   Number_of_genesets = dim(PANTHER)[1])
  PANTHER_OBJ <- synStore(PANTHER_OBJ, used = c('http://bioconductor.org/packages/3.0/data/annotation/src/contrib/PANTHER.db_3.0.0.tar.gz',
                                                'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                          activityName='Genesets Curation', executed = executed, activityDescription='PANTHER gene sets curation')
}

##############################
#### BIOCARTA MSigDB v4.0 ####
##############################
# Get MSig database (http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c2.cp.biocarta.v4.0.symbols.gmt)
BIOCARTA <- GSA.read.gmt('./c2.cp.biocarta.v4.0.symbols.gmt')

tmp1 <- ldply(sapply(BIOCARTA[[1]],paste,collapse='|'));
tmp2 <- ldply(BIOCARTA[[2]])

BIOCARTA <- cbind(tmp2,tmp1)
names(BIOCARTA) <- c('Name','symBeforeOverlap')
BIOCARTA <- as.data.table(BIOCARTA)

BIOCARTA <- BIOCARTA[,list("Source Id" = 'Not Available',Datasource = 'MSigDBv4.0', 
                           IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
BIOCARTA <- BIOCARTA[,list(Name = gsub('BIOCARTA_','biocarta:',Name)), by= setdiff(colnames(BIOCARTA),'Name')]
setcolorder(BIOCARTA,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'))        

# Write data to file and synapse
write.table(BIOCARTA, file = paste('./',sourceFolderName,'/BIOCARTA_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  BIOCARTA_OBJ <- File(paste('./',sourceFolderName,'/BIOCARTA_source.tsv',sep=''), name='BIOCARTA',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(BIOCARTA_OBJ) <- list(DataSource = 'MSigDBv4.0',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                    Description = 'Biocarta from C2 sets of MSigDB v4.0',Number_of_genesets = dim(BIOCARTA)[1])
  BIOCARTA_OBJ <- synStore(BIOCARTA_OBJ, used = c('http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c2.cp.biocarta.v4.0.symbols.gmt'),
                           activityName='Genesets Curation', executed = executed, activityDescription='Source gene sets from C2 sets of MSigDB v.4.0 database')
}

###################
#### NIH - PID ####
###################
# Get PID from Pathwaycommons database (http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.6.All.GSEA.gmt.gz)
PID <- GSA.read.gmt('./Pathway Commons.6.All.GSEA.gmt')

tmp1 <- ldply(sapply(PID[[1]],paste,collapse='|'));
tmp2 <- ldply(PID[[2]])
tmp3 <- ldply(sapply(PID[[3]],strsplit,';'))[,2:4];

PID <- as.data.table(cbind(tmp2,tmp1,tmp3))
setnames(PID,c('Name','uniprotBeforeOverlap','Datasource','Organism','IdType'))
PID <- PID[Datasource == "datasource: pid",][Organism == " organism: 9606",]

PID <- PID[,list(Datasource = 'PathwayCommons2',
                 Organism = gsub('organism: ','',Organism),
                 IdType = gsub('id type: ','',IdType),
                 'Source Id' = 'Not Available'), by=c('Name','uniprotBeforeOverlap')]
PID <- PID[,list(Name = paste(Datasource,strsplit(Name,':')[[1]][2],sep=':')),
                 by = c('Source Id','uniprotBeforeOverlap','Datasource','Organism','IdType')]

# Convert uniprot ids to symbols
ent2uni <- as.data.table(as.data.frame(org.Hs.egUNIPROT))
ent2sym <- as.data.table(as.data.frame(org.Hs.egSYMBOL))
uni2sym <- merge(ent2uni,ent2sym,by='gene_id')
uni2sym <- uni2sym[,list(symbol=paste(unique(symbol),collapse='|')),by='uniprot_id']
setkey(uni2sym,'uniprot_id')

# Get gene symbols from uniprot ids
uni2symMAPPER <- function(uniId,uni2sym){
  uniId <- unlist(strsplit(unlist(uniId),'\\|'))
  paste(unlist(uni2sym[uniId,'symbol',with=F]),collapse="|") 
  #   return(as.character(UNIPROT[idx,'SYMBOL',with=F]))
}
PID <- PID[,c('symBeforeOverlap') := uni2symMAPPER(.SD,uni2sym),
           by=c('Name','Source Id','Datasource','Organism','IdType'),.SDcols ='uniprotBeforeOverlap']
setcolorder(PID,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism','uniprotBeforeOverlap'))          

# Save source list in local folder and in synapse
write.table(PID, file = paste('./',sourceFolderName,'/PID_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  PID_OBJ <- File(paste('./',sourceFolderName,'/PID_source.tsv',sep=''), name='PID', parentId = SOURCE_FOLDER$properties$id, 
                  synapseStore=TRUE)
  annotations(PID_OBJ) <- list(DataSource = 'Pathway Commons 2',Curated = 'NO',Organism = 'ALL', IdType = 'Symbol', 
                               Description = 'PID gene sets from Pathway Commons 2 database', Number_of_genesets = dim(PID)[1])
  PID_OBJ <- synStore(PID_OBJ, used = c('http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.6.All.GSEA.gmt.gz',
                                        'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                      activityName='Genesets Curation', executed = executed, activityDescription='Source gene sets curation')
}

##################
#### HUMANCYC ####
##################
# Get HUMANCYC from Pathwaycommons database (http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.6.All.GSEA.gmt.gz)
HUMANCYC <- GSA.read.gmt('./Pathway Commons.6.All.GSEA.gmt')

tmp1 <- ldply(sapply(HUMANCYC[[1]],paste,collapse='|'));
tmp2 <- ldply(HUMANCYC[[2]])
tmp3 <- ldply(sapply(HUMANCYC[[3]],strsplit,';'))[,2:4];

HUMANCYC <- as.data.table(cbind(tmp2,tmp1,tmp3))
setnames(HUMANCYC,c('Name','uniprotBeforeOverlap','Datasource','Organism','IdType'))
HUMANCYC <- HUMANCYC[Datasource == "datasource: humancyc",][Organism == " organism: 9606",]

HUMANCYC <- HUMANCYC[,list(Datasource = 'PathwayCommons2',
                 Organism = gsub('organism: ','',Organism),
                 IdType = gsub('id type: ','',IdType),
                 'Source Id' = 'Not Available'), by=c('Name','uniprotBeforeOverlap')]
HUMANCYC <- HUMANCYC[,list(Name = paste(Datasource,strsplit(Name,':')[[1]][2],sep=':')),
           by = c('Source Id','uniprotBeforeOverlap','Datasource','Organism','IdType')]

# Convert uniprot ids to symbols
ent2uni <- as.data.table(as.data.frame(org.Hs.egUNIPROT))
ent2sym <- as.data.table(as.data.frame(org.Hs.egSYMBOL))
uni2sym <- merge(ent2uni,ent2sym,by='gene_id')
uni2sym <- uni2sym[,list(symbol=paste(unique(symbol),collapse='|')),by='uniprot_id']
setkey(uni2sym,'uniprot_id')

# Get gene symbols from uniprot ids
uni2symMAPPER <- function(uniId,uni2sym){
  uniId <- unlist(strsplit(unlist(uniId),'\\|'))
  paste(unlist(uni2sym[uniId,'symbol',with=F]),collapse="|") 
  #   return(as.character(UNIPROT[idx,'SYMBOL',with=F]))
}
HUMANCYC <- HUMANCYC[,c('symBeforeOverlap') := uni2symMAPPER(.SD,uni2sym),
           by=c('Name','Source Id','Datasource','Organism','IdType'),.SDcols ='uniprotBeforeOverlap']
setcolorder(HUMANCYC,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism','uniprotBeforeOverlap'))          

# Save source list in local folder and in synapse
write.table(HUMANCYC, file = paste('./',sourceFolderName,'/HUMANCYC_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  HUMANCYC_OBJ <- File(paste('./',sourceFolderName,'/HUMANCYC_source.tsv',sep=''), name='HUMANCYC', parentId = SOURCE_FOLDER$properties$id, 
                  synapseStore=TRUE)
  annotations(HUMANCYC_OBJ) <- list(DataSource = 'Pathway Commons 2',Curated = 'NO',Organism = 'ALL', IdType = 'Symbol', 
                               Description = 'HUMANCYC gene sets from Pathway Commons 2 database', Number_of_genesets = dim(HUMANCYC)[1])
  HUMANCYC_OBJ <- synStore(HUMANCYC_OBJ, used = c('http://www.pathwaycommons.org/pc2/downloads/Pathway%20Commons.6.All.GSEA.gmt.gz',
                                        'http://www.bioconductor.org/packages/release/data/annotation/src/contrib/org.Hs.eg.db_3.0.0.tar.gz'),
                      activityName='Genesets Curation', executed = executed, activityDescription='Source gene sets curation')
}

##############################
#### TRANSFAC MSigDB v4.0 ####
##############################
# Get MSig database (http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c3.tft.v4.0.symbols.gmt)
TRANSFAC <- GSA.read.gmt('./c3.tft.v4.0.symbols.gmt')

tmp1 <- ldply(sapply(TRANSFAC[[1]],paste,collapse='|'));
tmp2 <- ldply(TRANSFAC[[2]])

TRANSFAC <- cbind(tmp2,tmp1)
names(TRANSFAC) <- c('Name','symBeforeOverlap')
TRANSFAC <- as.data.table(TRANSFAC)

TRANSFAC <- TRANSFAC[,list("Source Id" = "Not Available", Datasource = 'MSigDBv4.0', 
                           IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
TRANSFAC <- TRANSFAC[,list(Name = paste('transfac',Name,sep=':')),
                     by = c('Source Id','symBeforeOverlap','Datasource','Organism','IdType')]
setcolorder(TRANSFAC,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism')) 

# Write data to file and synapse
write.table(TRANSFAC, file = paste('./',sourceFolderName,'/TRANSFAC_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  TRANSFAC_OBJ <- File(paste('./',sourceFolderName,'/TRANSFAC_source.tsv',sep=''), name='TRANSFAC',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(TRANSFAC_OBJ) <- list(DataSource = 'MSigDB v4.0',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                    Description = 'Transfac gene sets from C3 sets of MSigDB v4.0',
                                    Number_of_genesets = dim(TRANSFAC)[1])
  TRANSFAC_OBJ <- synStore(TRANSFAC_OBJ, used = c('http://www.broadinstitute.org/gsea/msigdb/download_file.jsp?filePath=/resources/msigdb/4.0/c3.tft.v4.0.symbols.gmt'),
                           activityName='Genesets Curation', executed = executed, activityDescription='Genesets Curation')
}

####################
#### TargetScan ####
####################
# Get Targetscan database (http://www.targetscan.org/vert_61/vert_61_data_download/Summary_Counts.txt.zip)
TARGETSCAN <- read.table('Summary_Counts.txt',sep='\t',quote='',header=T)
TARGETSCAN <- data.table(TARGETSCAN)

TARGETSCAN <- TARGETSCAN[Species.ID == '9606',]
TARGETSCAN <- TARGETSCAN[,list(Gene.Symbol = paste(unique(Gene.Symbol),collapse='|'),
                               miRNA.family = paste(unique(miRNA.family),sep='|')),
                         by = Representative.miRNA]
setnames(TARGETSCAN,c('Gene.Symbol','Representative.miRNA'),c('symBeforeOverlap','Name'))

TARGETSCAN <- TARGETSCAN[,list(Name = paste('targetscan',paste(Name,miRNA.family,sep='|'),sep=':'),
                               "Source Id" = "Not Available", Datasource = 'MSigDBv4.0', IdType = 'symbol', 
                               Organism = 'human'),by=c('symBeforeOverlap')]
TARGETSCAN <- TARGETSCAN[,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'),with=F]
setcolorder(TARGETSCAN,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism')) 

# Write data to file and synapse
write.table(TARGETSCAN, file = paste('./',sourceFolderName,'/TARGETSCAN_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  TARGETSCAN_OBJ <- File(paste('./',sourceFolderName,'/TARGETSCAN_source.tsv',sep=''), name='TARGETSCAN',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(TARGETSCAN_OBJ) <- list(DataSource = 'MSigDBv4.0',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                      Description = 'miRNA target sets from Targetscan R6.2 a subset of MSigDBv4.0',
                                      Number_of_genesets = dim(TARGETSCAN)[1])
  TARGETSCAN_OBJ <- synStore(TARGETSCAN_OBJ, used = c('http://www.targetscan.org/vert_61/vert_61_data_download/Summary_Counts.txt.zip'),
                             activityName='Genesets Curation', executed = executed, activityDescription='Genesets Curation')
}

#######################################################
####  Genebook : SCZ subset (from Menachem Former) ####
#######################################################
# Get subset of curated genebook sets from synapse (syn3221231)
GENEBOOK_SOURCE_OBJ <- synGet('syn3221231')
GENEBOOK <- read.table(getFileLocation(GENEBOOK_SOURCE_OBJ),sep='\t',quote='',header=T)
GENEBOOK <- data.table(GENEBOOK)

GENEBOOK <- GENEBOOK[,list(Name = paste(Category,Set,sep=':')), by = Gene]
GENEBOOK <- GENEBOOK[,list(symBeforeOverlap = paste(unique(Gene),collapse='|')), by = Name]

GENEBOOK <- GENEBOOK[,list("Source Id" = "Not Available", Datasource = 'GeneBook', 
                           IdType = 'symbol', Organism = 'human'), by=c('Name','symBeforeOverlap')]
GENEBOOK <- GENEBOOK[,c('Name','symBeforeOverlap','Source Id','Datasource','IdType','Organism'),with=F]

# Write data to file and synapse
write.table(GENEBOOK, file = paste('./',sourceFolderName,'/GENEBOOK_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  GENEBOOK_OBJ <- File(paste('./',sourceFolderName,'/GENEBOOK_source.tsv',sep=''), name='GENEBOOK',
                       parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(GENEBOOK_OBJ) <- list(DataSource = 'Genebook',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                    Description = 'Retained CMC related subsets for analysis from genebook',
                                    Number_of_genesets = dim(GENEBOOK)[1])
  GENEBOOK_OBJ <- synStore(GENEBOOK_OBJ, used = GENEBOOK_SOURCE_OBJ, activityName='Genesets Curation', 
                           executed = executed, activityDescription='Retained subsets from Genebook')
}

####################################################################
####  Cell type marker and other gene sets (from Panos Roussos) ####
###################################################################
# Get subset of curated cell type markers from synapse (syn3159415; file name = GENE_ANNOT_AllGenes.tsv)
CELLMARKERS_SOURCE_OBJ <- synGet('syn3159415')
CELLMARKERS <- read.table(getFileLocation(CELLMARKERS_SOURCE_OBJ),sep='\t',quote='',header=T)
CELLMARKERS <- data.table(CELLMARKERS)

CELLMARKERS <- CELLMARKERS[,list(symBeforeOverlap = paste(unique(Gene),collapse='|')), by = c('Set','Category')]
CELLMARKERS <- CELLMARKERS[,list(Set = paste(Category,Set,sep=':')), by = symBeforeOverlap]
setnames(CELLMARKERS,'Set','Name')

CELLMARKERS <- CELLMARKERS[,list("Source Id" = "Not Available",Datasource = 'NotKnown', IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
CELLMARKERS <- CELLMARKERS[,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'),with=F]

# Write data to file and synapse
write.table(CELLMARKERS, file = paste('./',sourceFolderName,'/CELLMARKERS_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  CELLMARKERS_OBJ <- File(paste('./',sourceFolderName,'/CELLMARKERS_source.tsv',sep=''), name='CELLMARKERS',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(CELLMARKERS_OBJ) <- list(DataSource = 'Cell Markers',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                       Description = 'Retained subsets of cell type markers',
                                       Number_of_genesets = dim(CELLMARKERS)[1])
  CELLMARKERS_OBJ <- synStore(CELLMARKERS_OBJ, used = CELLMARKERS_SOURCE_OBJ, activityName='Genesets Curation', 
                           executed = executed, activityDescription='Gene sets for cell type markers')
}

######################################################################
####  Cell type markers from Zhang gene sets (from Panos Roussos) ####
######################################################################
# Get subset of curated cell type markers from synapse (syn3226828; file name = Zhang_Gene_set.tsv)
ZHANGGENESETS_SOURCE_OBJ <- synGet('syn3226828')
ZHANGGENESETS <- read.table(getFileLocation(ZHANGGENESETS_SOURCE_OBJ),sep='\t',quote='',header=T)
ZHANGGENESETS <- data.table(ZHANGGENESETS)

ZHANGGENESETS <- ZHANGGENESETS[,list(symBeforeOverlap = paste(unique(Gene),collapse='|')), by = c('Set','Category')]
ZHANGGENESETS <- ZHANGGENESETS[,list(Set = paste(Category,Set,sep=':')), by = symBeforeOverlap]
setnames(ZHANGGENESETS,'Set','Name')

ZHANGGENESETS <- ZHANGGENESETS[,list("Source Id" = "Not Available",Datasource = 'NotKnown', IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
ZHANGGENESETS <- ZHANGGENESETS[,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'),with=F]

# Write data to file and synapse
write.table(ZHANGGENESETS, file = paste('./',sourceFolderName,'/ZHANGGENESETS_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  ZHANGGENESETS_OBJ <- File(paste('./',sourceFolderName,'/ZHANGGENESETS_source.tsv',sep=''), name='ZHANGGENESETS',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(ZHANGGENESETS_OBJ) <- list(DataSource = 'Cell Markers',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                         Description = 'Retained subsets of cell type markers from Zhang gene sets',
                                         Number_of_genesets = dim(ZHANGGENESETS)[1])
  ZHANGGENESETS_OBJ <- synStore(ZHANGGENESETS_OBJ, used = ZHANGGENESETS_SOURCE_OBJ, activityName='Genesets Curation', 
                                executed = executed, activityDescription='Gene sets for cell type markers')
}

################################################################
####  Secondary hypothesis sets of ASD (from Panos Roussos) ####
################################################################
# Get subset of curated cell type markers from synapse (syn3270346; file name = GENE_ANNOT_ASD.tsv)
ASD_SOURCE_OBJ <- synGet('syn3270346')
ASD <- read.table(getFileLocation(ASD_SOURCE_OBJ),sep='\t',quote='',header=T)
ASD <- data.table(ASD)

ASD <- ASD[,list(symBeforeOverlap = paste(unique(Gene),collapse='|')), by = c('Set','Category')]
ASD <- ASD[,list(Set = paste(Category,Set,sep=':')), by = symBeforeOverlap]
setnames(ASD,'Set','Name')

ASD <- ASD[,list("Source Id" = "Not Available",Datasource = 'NotKnown', IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
ASD <- ASD[,c('Name','Source Id','symBeforeOverlap','Datasource','IdType','Organism'),with=F]

# Write data to file and synapse
write.table(ASD, file = paste('./',sourceFolderName,'/ASD_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  ASD_OBJ <- File(paste('./',sourceFolderName,'/ASD_source.tsv',sep=''), name='ASD',parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(ASD_OBJ) <- list(DataSource = 'Cell Markers',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                         Description = 'Curated ASD markers',
                                         Number_of_genesets = dim(ASD)[1])
  ASD_OBJ <- synStore(ASD_OBJ, used = ASD_SOURCE_OBJ, activityName='Genesets Curation', 
                                executed = executed, activityDescription='Gene sets for cell type markers')
}


############################################
####  Neuropsych (from Menachem Former) ####
############################################
# Get curated non-scz but related to other neuro gene sets from synapse (syn3232199)
NEUROPSYCH_SOURCE_OBJ <- synGet('syn3232199')
NEUROPSYCH <- read.table(getFileLocation(NEUROPSYCH_SOURCE_OBJ),sep='\t',quote='',header=T)
NEUROPSYCH <- data.table(NEUROPSYCH)

NEUROPSYCH <- NEUROPSYCH[,list(Name = paste(Category,Set,sep=':')), by = Gene]
NEUROPSYCH <- NEUROPSYCH[,list(symBeforeOverlap = paste(unique(Gene),collapse='|')), by = Name]

NEUROPSYCH <- NEUROPSYCH[,list("Source Id" = "Not Available", Datasource = 'NotKnown', 
                               IdType = 'symbol', Organism = 'human'),by=c('Name','symBeforeOverlap')]
NEUROPSYCH <- NEUROPSYCH[,c('Name',"Source Id",'symBeforeOverlap','Datasource','IdType','Organism'),with=F]

# Write data to file and synapse
write.table(NEUROPSYCH, file = paste('./',sourceFolderName,'/NEUROPSYCH_source.tsv',sep=''), sep='\t', quote=F, row.names=F)

if (SYNAPSE_STORE){
  NEUROPSYCH_OBJ <- File(paste('./',sourceFolderName,'/NEUROPSYCH_source.tsv',sep=''), name='NEUROPSYCH',
                         parentId = SOURCE_FOLDER$properties$id, synapseStore=TRUE)
  annotations(NEUROPSYCH_OBJ) <- list(DataSource = 'NotKnown',Curated = 'NO',Organism = 'Human', IdType = 'Symbol', 
                                      Description = 'Gene sets for neuro psychiatric disorders (other than scz)',
                                      Number_of_genesets = dim(NEUROPSYCH)[1])
  NEUROPSYCH_OBJ <- synStore(NEUROPSYCH_OBJ, used = NEUROPSYCH_SOURCE_OBJ, activityName='Genesets Curation', 
                              executed = executed, activityDescription='Gene sets for neuro psychiatric disorders (other than scz)')
}

##################################################################################################################################
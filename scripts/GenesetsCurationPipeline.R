##################################################################################################################################
#### Curate gene sets for enrichment analysis ####
# Clear Screen
cat("\014")  

# Clear workspace
rm(list=ls())

# Load external libraries
library('synapseClient')
library('data.table')

# Login to synapse
synapseLogin()
##################################################################################################################################


##################################################################################################################################
#### Synapse Parameters ####
SYNAPSE_STORE = T;
sourceId = 'syn3240583';
parentId = 'syn3168452';
geneMappingId = 'syn2713767';

BRAIN_REGION_NAME = 'DLPFC';

MIN_SIZE = 10;
MAX_SIZE = 1000;

executed = 'https://bitbucket.org/commonmind/commonmind/src/92b6370b21ab1a627907e9ed6fd7384d1c495504/scripts/phaseI/GenesetsCurationPipeline.R?at=master';

curatedFolderName1 = paste('GeneSets',BRAIN_REGION_NAME,'Overlapped',sep='_');
curatedFolderName2 = paste('GeneSets',BRAIN_REGION_NAME,'Overlapped','SizeFiltered',sep='_');
curatedFolderName3 = paste('GeneSets',BRAIN_REGION_NAME,'Overlapped','SizeFiltered','Merged',sep='_');
##################################################################################################################################


##################################################################################################################################
#### Get Entities from synapse ####

# Get expressed genes list
DE_OBJ <- synGet(geneMappingId)
DE <- read.table(getFileLocation(DE_OBJ),header=T,row.names=1)
##################################################################################################################################



##################################################################################################################################
#### Filter gene sets with atleast 10% of the genes are expressed in DLPFC ####
# Create directory to store filtered files in local folder
system(paste('mkdir ./',curatedFolderName1,sep=''))

# Create directory to store filtered files in synapse folder
if (SYNAPSE_STORE){
  # Create folder in synapse
  CURATED_FOLDER1 <- Folder(name = curatedFolderName1, parentId = parentId)
  annotations(CURATED_FOLDER1) <- list(Curated = 'YES',Organism = 'Human', IdType = 'Symbol', 
                                       Description = 'Curated genesets with atleast 10% of the genes are expressed in DLPFC')
  CURATED_FOLDER1 <- synStore(CURATED_FOLDER1)
}

filterExpressedGenes <- function(ENTITY,ExpressedGenes,PARENT_OBJ,PARENT_FOLDER,
                                 PercentOverlapped=0.1, ANNOTATIONS=list(),USED=c(),EXECUTED=c(),ACTIVITY_DESCRIPTION=c(),SYNAPSE_STORE=F){
  # Download GSET from synapse
  GSET_OBJ <- synGet(ENTITY['file.id'])
  GSET <- read.table(getFileLocation(GSET_OBJ),sep='\t',header=T,stringsAsFactors=F,quote="",check.names=F,fill=T)
  GSET <- data.table(GSET)
  
  # Name of GSET
  GSET_NAME <- as.character(ENTITY['file.name'])
  ANNOTATIONS=c(ANNOTATIONS,list(DataSource = as.character(ENTITY['file.DataSource']), Curated = 'YES', Organism = 'Human',
                                 IdType = 'Symbol', Description = paste(ENTITY['file.Description'],'overlapped with DLPFC expressed genes',sep=' ')));
  USED=c(USED,GSET_OBJ);
  ACTIVITY_DESCRIPTION='Filtering DLPFC expressed genes';
    
  # Function to calculate overlap percentage
  findOverlap <- function(cols,mappedGENES){
    cols = unlist(strsplit(unlist(cols),'\\|'))
    cols = unique(cols[!is.na(cols)])
    szBeforeOverlap = length(cols)
    szAfterOverlap = length(intersect(cols,mappedGENES))
    symAfterOverlap = paste(intersect(cols,mappedGENES),collapse='|')
    percentOverlapped = szAfterOverlap/szBeforeOverlap
    return(list(symAfterOverlap,szBeforeOverlap,szAfterOverlap,percentOverlapped))  
  }
  
  # Filter GSET
  GSET <- GSET[,c('symAfterOverlap','szBeforeOverlap','szAfterOverlap','percentOverlapped') := findOverlap(.SD,ExpressedGenes),
           by=c('Name','Source Id','Datasource','IdType','Organism'),.SDcols='symBeforeOverlap']
  GSET <- GSET[,][percentOverlapped > PercentOverlapped]
  
  # Save GSET
  write.table(GSET, file = paste('./',PARENT_FOLDER,'/',GSET_NAME,'_DLPFC_Overlapped.tsv',sep=''), sep='\t', quote=F, row.names=F)
  
  # Store in synapse
  if (SYNAPSE_STORE){
    GSET_OBJ <- File(paste('./',PARENT_FOLDER,'/',GSET_NAME,'_DLPFC_Overlapped.tsv',sep=''), name = GSET_NAME,
                    parentId = PARENT_OBJ$properties$id, synapseStore=TRUE)
    annotations(GSET_OBJ) <- c(ANNOTATIONS,list(Number_of_genesets = dim(GSET)[1]))
    GSET_OBJ <- synStore(GSET_OBJ, used = USED, activityName='Genesets Curation: DLPFC Filtering', executed = EXECUTED, 
                        activityDescription=ACTIVITY_DESCRIPTION)
    return(list(GSET= GSET, GSET_OBJ = GSET_OBJ))
  }
  return(list(GSET = GSET, GSET_OBJ = list()))
}
  
# Get all entities from source, curate and store them back in synapse
ALL.ENTITY <- synQuery(paste('select * from file where parentId=="',sourceId,'"',sep=''))
ALL.ENTITY <- ALL.ENTITY[ALL.ENTITY$file.name %in% c('GENEANNOT.SCZ','GENEANNOT.ASD','SCZ.GENETICS'),]

CURATED_SETS1 = apply(ALL.ENTITY,1,filterExpressedGenes,DE$MAPPED_genes,CURATED_FOLDER1,curatedFolderName1,
                      PercentOverlapped=0.1, USED=c(DE_OBJ),EXECUTED=executed,SYNAPSE_STORE=SYNAPSE_STORE)
##################################################################################################################################




##################################################################################################################################
#### Filter gene sets with size between 10 and 1000 ####
# Create directory
system(paste('mkdir ./',curatedFolderName2,sep=''))

if (SYNAPSE_STORE){
  # Create folder in synapse
  CURATED_FOLDER2 <- Folder(name = curatedFolderName2, parentId = parentId)
  annotations(CURATED_FOLDER2) <- list(Curated = 'YES',Organism = 'Human', IdType = 'Symbol', 
                                       Description = 'Curated genesets with size between 10 and 1000 genes')
  CURATED_FOLDER2 <- synStore(CURATED_FOLDER2)
}

# Function to filter gene sets of size > minSz and < maxSz
filterSizeGsets <- function(ENTITY,PARENT_OBJ,PARENT_FOLDER,
                            minSz=10,maxSz=1000,ANNOTATIONS=list(),USED=c(),EXECUTED=c(),ACTIVITY_DESCRIPTION=c(),SYNAPSE_STORE=F){
  
  # Download GSET from synapse
  GSET_OBJ <- synGet(ENTITY['file.id'])
  GSET <- read.table(getFileLocation(GSET_OBJ),sep='\t',header=T,stringsAsFactors=F,quote="",check.names=F,fill=T)
  GSET <- data.table(GSET)
  
  # Name of GSET
  GSET_NAME <- as.character(ENTITY['file.name']);    
  ANNOTATIONS=c(ANNOTATIONS,list(DataSource = as.character(ENTITY['file.DataSource']), Curated = 'YES', 
                                 Organism = 'Human', IdType = 'Symbol', 
                                 Description = paste(ENTITY['file.Description'],'overlapped with DLPFC expressed genes and size filtered to be between 10 and 1000 genes',sep=' ')));
  USED=c(USED,GSET_OBJ);
  ACTIVITY_DESCRIPTION='Filtering gene sets with size between 10 and 1000';
  
  # Filter GSET
  GSET <- GSET[,][szAfterOverlap >= minSz & szAfterOverlap <= maxSz]
  
  # Save GSET
  write.table(GSET, file = paste('./',PARENT_FOLDER,'/',GSET_NAME,'_SizeFiltered.tsv',sep=''), sep='\t', quote=F, row.names=F)
  
  # Store in synapse
  if (SYNAPSE_STORE){
    GSET_OBJ <- File(paste('./',PARENT_FOLDER,'/',GSET_NAME,'_SizeFiltered.tsv',sep=''), name = GSET_NAME,
                     parentId = PARENT_OBJ$properties$id, synapseStore=TRUE)
    annotations(GSET_OBJ) <- c(ANNOTATIONS,list(Number_of_genesets = dim(GSET)[1]))
    GSET_OBJ <- synStore(GSET_OBJ, used = USED, activityName='Genesets Curation: Size Filtering', executed = EXECUTED, 
                         activityDescription=ACTIVITY_DESCRIPTION)
    return(list(GSET = GSET, GSET_OBJ = GSET_OBJ))
  }
  return(list(GSET = GSET, GSET_OBJ = list()))
}

# Get all entities from source, curate and store them back in synapse
ALL.ENTITY <- synQuery(paste('select * from file where parentId=="',CURATED_FOLDER1$properties$id,'"',sep=''))
ALL.ENTITY <- ALL.ENTITY[ALL.ENTITY$file.name %in% c('GENEANNOT.SCZ','GENEANNOT.ASD','SCZ.GENETICS'),]

CURATED_SETS2 = apply(ALL.ENTITY,1,filterSizeGsets,CURATED_FOLDER2,curatedFolderName2,
                      minSz=MIN_SIZE,maxSz=MAX_SIZE,EXECUTED=executed,SYNAPSE_STORE=SYNAPSE_STORE)
##################################################################################################################################




##################################################################################################################################
#### Merge gene sets together to form hypothesis free and driven sets ####
# Create directory
system(paste('mkdir ./',curatedFolderName3,sep=''))

if (SYNAPSE_STORE){
  # Create folder in synapse
  CURATED_FOLDER3 <- Folder(name = curatedFolderName3, parentId = parentId)
  annotations(CURATED_FOLDER3) <- list(Curated = 'YES',Organism = 'Human', IdType = 'Symbol', 
                                       Description = 'Merged gene sets')
  CURATED_FOLDER3 <- synStore(CURATED_FOLDER3)
}

#### Function to merge gene sets
mergeGSETS <- function(primENTITY,secENTITY,GSET_NAME,PARENT_OBJ,PARENT_FOLDER,
                       maxJI=0.5,ANNOTATIONS=list(),USED=c(),EXECUTED=c(),ACTIVITY_DESCRIPTION=c(),SYNAPSE_STORE=F){
  # Download primary GSET from synapse
  primGSET_OBJ <- synGet(primENTITY['file.id'])
  primGSET <- read.table(getFileLocation(primGSET_OBJ),sep='\t',header=T,stringsAsFactors=F,quote="",na.strings="",fill=T)
  primGSET <- data.table(primGSET)
  
  # Download secondary GSET from synapse
  secGSET_OBJ <- synGet(secENTITY['file.id'])
  secGSET <- read.table(getFileLocation(secGSET_OBJ),sep='\t',header=T,stringsAsFactors=F,quote="",na.strings="")
  secGSET <- data.table(secGSET)
  
  # Synapse parameters
  ANNOTATIONS=c(ANNOTATIONS,list(DataSource = 'In house curation', Curated = 'YES', Organism = 'Human',IdType = 'Symbol', 
                                   Description = paste(GSET_NAME,'gene sets, merged with a Jaccard index of less than',maxJI,sep=' ')));
  USED = unique(c(USED,secGSET_OBJ));
  EXECUTED = executed;
  ACTIVITY_DESCRIPTION = paste(ACTIVITY_DESCRIPTION,'Merged gene sets',sep='|');
  
  # Combine primGSET and secGSET with Jaccard index less than minJI
  calculateOverlap <- function(secGSET,primGSET){
    secGSET = unlist(strsplit(unlist(secGSET),'\\|'))
    secGSET = secGSET[!is.na(secGSET)]
    primGSET = as.data.frame(primGSET[,'symAfterOverlap',with=F])
    
    calculateJI <- function(primGSET,secGSET){
      primGSET = unlist(strsplit(primGSET,'\\|'))
      primGSET = primGSET[!is.na(primGSET)]
      JI = length(intersect(primGSET,secGSET))/length(unique(c(primGSET,secGSET)))
      return(JI)
    }
    
    return(max(apply(primGSET,1,calculateJI,secGSET)))
  }
  
  # select sets with JI < maxJI
  secGSET <- secGSET[,c('JaccardIndex') := calculateOverlap(.SD,primGSET),
                           by=setdiff(colnames(secGSET),'symAfterOverlap'),.SDcols = 'symAfterOverlap']
  secGSET <- secGSET[,][JaccardIndex<=maxJI]
  
  # Merge gene sets
  GSET <- merge(primGSET,secGSET,by=c("Name","symBeforeOverlap","szBeforeOverlap","symAfterOverlap",
                                      "szAfterOverlap","percentOverlapped","Datasource"),all=T)
  
  GSET <- GSET[,c("Name","symBeforeOverlap","szBeforeOverlap","symAfterOverlap","szAfterOverlap","percentOverlapped","Datasource"),with=F]
  
  # Write gene sets to file
  write.table(GSET, file = paste('./',PARENT_FOLDER,'/',GSET_NAME,'.tsv',sep=''), sep='\t', quote=F, row.names=F)
  
  # Store file in synapse
  if (SYNAPSE_STORE){
    GSET_OBJ <- File(paste('./',PARENT_FOLDER,'/',GSET_NAME,'.tsv',sep=''),name=GSET_NAME, parentId = PARENT_OBJ$properties$id, 
                     synapseStore=TRUE)
    annotations(GSET_OBJ) <- c(ANNOTATIONS,list(Number_of_genesets = dim(GSET)[1]))
    GSET_OBJ <- synStore(GSET_OBJ, used = USED, activityName='Genesets Curation: Merging',
                         executed = EXECUTED, activityDescription = ACTIVITY_DESCRIPTION)
    return(list(GSET=GSET,GSET_OBJ=GSET_OBJ))
  }
  return(list(GSET=GSET,GSET_OBJ=list()))
}

# Get all entities from merge and store them back in synapse
ALL.ENTITY <- synQuery(paste('select * from file where parentId=="',CURATED_FOLDER2$properties$id,'"',sep=''))

#### Get hypothesis free set ####
HYP_FREE_SET = c('GO','GENEFAMILY','REACTOME')
primENTITY <- as.character(ALL.ENTITY[grep(HYP_FREE_SET[1],ALL.ENTITY$file.name),,drop=F])
names(primENTITY) <- colnames(ALL.ENTITY)
USED = synGet(primENTITY['file.id'],downloadFile=F)
DATASOURCE = as.character(primENTITY['file.DataSource'])

for (i in 2:length(HYP_FREE_SET)){
  secENTITY <- as.character(ALL.ENTITY[grep(HYP_FREE_SET[i],ALL.ENTITY$file.name),,drop=F])
  names(secENTITY) <- colnames(ALL.ENTITY)  
  USED = c(USED,synGet(secENTITY['file.id'],downloadFile=F))
  DATASOURCE = paste(DATASOURCE,as.character(secENTITY['file.DataSource']),sep=';')
  
  GSET_NAME = 'HYPOTHESIS_FREE'
  HYP_FREE_OBJ <- mergeGSETS(primENTITY,secENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=0.5,
                             ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
  
  tmp = synQuery(paste('select * from file where parentId == "',CURATED_FOLDER3$properties$id,'" and name == "',GSET_NAME,'"',sep=''))
  primENTITY <- as.character(tmp[grep(GSET_NAME,tmp$file.name),,drop=F])
  names(primENTITY) <- colnames(tmp)
  
  print(paste(i,'sets merged',sep=' '))
}

#### Get hypothesis set for DE####
HYP_SET = c('SCZ.GENETICS')
primENTITY <- as.character(ALL.ENTITY[grep(HYP_SET[1],ALL.ENTITY$file.name),,drop=F])
names(primENTITY) <- colnames(ALL.ENTITY)
USED = synGet(primENTITY['file.id'],downloadFile=F)
DATASOURCE = as.character(primENTITY['file.DataSource'])

if (length(HYP_SET) <=1){
  GSET_NAME = 'HYPOTHESIS_DRIVEN_FOR_DE'
  HYP_OBJ <- mergeGSETS(primENTITY,primENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=0,
                        ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
}else{
  for (i in 2:length(HYP_SET)){
    secENTITY <- as.character(ALL.ENTITY[grep(HYP_SET[i],ALL.ENTITY$file.name),,drop=F])
    names(secENTITY) <- colnames(ALL.ENTITY)  
    USED = c(USED,synGet(secENTITY['file.id'],downloadFile=F))
    DATASOURCE = paste(DATASOURCE,as.character(secENTITY['file.DataSource']),sep=';')
    
    GSET_NAME = 'HYPOTHESIS_DRIVEN_FOR_DE'
    HYP_OBJ <- mergeGSETS(primENTITY,secENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=1,
                          ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
    
    tmp = synQuery(paste('select * from file where parentId == "',CURATED_FOLDER3$properties$id,'" and name == "',GSET_NAME,'"',sep=''))
    primENTITY <- as.character(tmp[grep(GSET_NAME,tmp$file.name),,drop=F])
    names(primENTITY) <- colnames(tmp)
    
    print(paste(i,'sets merged',sep=' ')) 
  }
}

#### Get hypothesis set for MODULES ####
HYP_SET = c('SCZ.GENETICS','GENEANNOT.SCZ')
primENTITY <- as.character(ALL.ENTITY[grep(HYP_SET[1],ALL.ENTITY$file.name),,drop=F])
names(primENTITY) <- colnames(ALL.ENTITY)
USED = synGet(primENTITY['file.id'],downloadFile=F)
DATASOURCE = as.character(primENTITY['file.DataSource'])

if (length(HYP_SET) <=1){
  GSET_NAME = 'HYPOTHESIS_DRIVEN_FOR_MODULES'
  HYP_OBJ <- mergeGSETS(primENTITY,primENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=0,
                        ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
}else{
  for (i in 2:length(HYP_SET)){
    secENTITY <- as.character(ALL.ENTITY[grep(HYP_SET[i],ALL.ENTITY$file.name),,drop=F])
    names(secENTITY) <- colnames(ALL.ENTITY)  
    USED = c(USED,synGet(secENTITY['file.id'],downloadFile=F))
    DATASOURCE = paste(DATASOURCE,as.character(secENTITY['file.DataSource']),sep=';')
  
    GSET_NAME = 'HYPOTHESIS_DRIVEN_FOR_MODULES'
    HYP_OBJ <- mergeGSETS(primENTITY,secENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=1,
                          ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
  
    tmp = synQuery(paste('select * from file where parentId == "',CURATED_FOLDER3$properties$id,'" and name == "',GSET_NAME,'"',sep=''))
    primENTITY <- as.character(tmp[grep(GSET_NAME,tmp$file.name),,drop=F])
    names(primENTITY) <- colnames(tmp)
  
    print(paste(i,'sets merged',sep=' '))
  }
}

#### Get secondary hypothesis sets for DE ####
SEC_HYP_SET = c('ASD.GENETICS')
primENTITY <- as.character(ALL.ENTITY[grep(SEC_HYP_SET[1],ALL.ENTITY$file.name),,drop=F])
names(primENTITY) <- colnames(ALL.ENTITY)
USED = synGet(primENTITY['file.id'],downloadFile=F)
DATASOURCE = as.character(primENTITY['file.DataSource'])

if (length(SEC_HYP_SET) <=1){
  GSET_NAME = 'SECONDARY_HYPOTHESIS_FOR_DE'
  HYP_OBJ <- mergeGSETS(primENTITY,primENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=0,
                        ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
}else{
  for (i in 2:length(SEC_HYP_SET)){
    secENTITY <- as.character(ALL.ENTITY[grep(SEC_HYP_SET[i],ALL.ENTITY$file.name),,drop=F])
    names(secENTITY) <- colnames(ALL.ENTITY)  
    USED = c(USED,synGet(secENTITY['file.id'],downloadFile=F))
    DATASOURCE = paste(DATASOURCE,as.character(secENTITY['file.DataSource']),sep=';')
    
    GSET_NAME = 'SECONDARY_HYPOTHESIS_FOR_DE'
    HYP_OBJ <- mergeGSETS(primENTITY,secENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=1,
                          ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
    
    tmp = synQuery(paste('select * from file where parentId == "',CURATED_FOLDER3$properties$id,'" and name == "',GSET_NAME,'"',sep=''))
    primENTITY <- as.character(tmp[grep(GSET_NAME,tmp$file.name),,drop=F])
    names(primENTITY) <- colnames(tmp)
    
    print(paste(i,'sets merged',sep=' '))
  }
}

#### Get secondary hypothesis sets for MODULES ####
SEC_HYP_SET = c('ASD.GENETICS','GENEANNOT.ASD')
primENTITY <- as.character(ALL.ENTITY[grep(SEC_HYP_SET[1],ALL.ENTITY$file.name),,drop=F])
names(primENTITY) <- colnames(ALL.ENTITY)
USED = synGet(primENTITY['file.id'],downloadFile=F)
DATASOURCE = as.character(primENTITY['file.DataSource'])

if (length(SEC_HYP_SET) <=1){
  GSET_NAME = 'SECONDARY_HYPOTHESIS_FOR_MODULES'
  HYP_OBJ <- mergeGSETS(primENTITY,primENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=0,
                        ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
}else{
  for (i in 2:length(SEC_HYP_SET)){
    secENTITY <- as.character(ALL.ENTITY[grep(SEC_HYP_SET[i],ALL.ENTITY$file.name),,drop=F])
    names(secENTITY) <- colnames(ALL.ENTITY)  
    USED = c(USED,synGet(secENTITY['file.id'],downloadFile=F))
    DATASOURCE = paste(DATASOURCE,as.character(secENTITY['file.DataSource']),sep=';')
    
    GSET_NAME = 'SECONDARY_HYPOTHESIS_FOR_MODULES'
    HYP_OBJ <- mergeGSETS(primENTITY,secENTITY,GSET_NAME,CURATED_FOLDER3,curatedFolderName3,maxJI=1,
                          ANNOTATIONS=list(Datasource = DATASOURCE),USED=USED,EXECUTED=executed, SYNAPSE_STORE = SYNAPSE_STORE)
    
    tmp = synQuery(paste('select * from file where parentId == "',CURATED_FOLDER3$properties$id,'" and name == "',GSET_NAME,'"',sep=''))
    primENTITY <- as.character(tmp[grep(GSET_NAME,tmp$file.name),,drop=F])
    names(primENTITY) <- colnames(tmp)
    
    print(paste(i,'sets merged',sep=' '))
  }
}
##################################################################################################################################



##################################################################################################################################
#### Find final number of gene sets ####
# Get all entities from merge and store them back in synapse
ALL.ENTITY <- synQuery(paste('select * from file where parentId=="',CURATED_FOLDER3$properties$id,'"',sep=''))

ALL <- integer()
for (i in 1:dim(ALL.ENTITY)[1]){
  tmp <- read.table(getFileLocation(synGet(ALL.ENTITY$file.id[i])),header=T,sep='\t',quote='');
  ALL<- c(ALL,summary(tmp$Datasource))
}
ALL <- ALL[sort(unique(names(ALL)))]

for (i in 1:length(ALL)){
  print(paste(names(ALL)[i],ALL[i],sep='|'))
}
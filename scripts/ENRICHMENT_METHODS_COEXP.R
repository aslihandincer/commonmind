####################################################################################################
# Implementations of various approaches for enrichemnt analysis for Differentially expressed genes #
####################################################################################################
library('goseq')
library('GO.db')
library('GSVA')
library('org.Hs.eg.db')
library('parallel')
library('limma')
library('topGO')

#############################################################################################################
GOSeq <- function(
  ###########################################################################################################
  #### Methods Specific Options ####
  GENEIDS = 'geneSymbol', #geneSymbol represents HGNC symbols, for other options run supportedGeneIDs() function
  GENOMES = 'hg19', #hg19 represents Genome reference consotium GRCh37 on Feb 2009, for other options run supportedGenomes() function 
  GO_CATEGORY = c("GO:CC","GO:BP","GO:MF"),
  DB = 'GO',
  GSET_LIST = NULL,
  METHOD = 'Wallenius' # other options are 'Sampling','Hypergeometric'
){
  
  calcEnrichment <- function(COEXP_OBJ,COEXP_MAT,DE_OBJ,DE_MAT,EXP_MAT,DESIGN_MAT,RESULTS_DIR,PVAL){
    ###################################################################################################################
    # Enrichment Analysis by GOSeq accounting for length variations of genes
    ###################################################################################################################
    FILE_NAME <- paste(RESULTS_DIR,gsub('.tsv',paste('',DB,'GOSeq.tsv',sep='_'),COEXP_OBJ$properties$name),sep='/')
    
    # Attach DExp Gene List to the user supplied Genelist
    tmp <- list(rownames(EXP_MAT)[DE_MAT$adj.P.Val<=0.05])
    names(tmp) <- as.character(DE_OBJ$properties$name)
    GSET_LIST <- c(GSET_LIST, tmp)
      
    ALL_RESULTS = data.frame()
    for (module in levels(COEXP_MAT[,'Module'])){
      print(module)
      # Inverse user supplied genelist from category - members to members-category for goseq
      if (!is.null(GSET_LIST))
        GSET_LIST = inverseList(GSET_LIST)
      
      # Gene selection
      allGenes = as.numeric(COEXP_MAT$Module == module)
      names(allGenes) = rownames(EXP_MAT)
      
      # Calculate probability weighting function based on length of gene
      pwf = nullp(allGenes,GENOMES,GENEIDS)
            
      # Calculate go enrichment using Wallenius method    
      GO.wall = goseq(pwf, GENOMES, GENEIDS, gene2cat=GSET_LIST, test.cats=GO_CATEGORY,
                      method = METHOD)
      GO.wall$over_represented_pvalue = p.adjust(GO.wall$over_represented_pvalue, method = 'BH')
      GO.wall$under_represented_pvalue = p.adjust(GO.wall$under_represented_pvalue, method = 'BH')
      rownames(GO.wall) <- GO.wall$category
      
      # Map results to GOTERM
      if (is.null(GSET_LIST)){
        RESULTS <- toTable(GOTERM)
        RESULTS <- cbind(RESULTS,GO.wall[RESULTS$go_id,])
        RESULTS <- RESULTS[!is.na(RESULTS$over_represented_pvalue),]
        RESULTS <- RESULTS[order(RESULTS$over_represented_pvalue),]
        RESULTS <- RESULTS[,-c(2,8)]
      } else {
        RESULTS <- GO.wall
        RESULTS <- RESULTS[!is.na(RESULTS$over_represented_pvalue),]
        RESULTS <- RESULTS[order(RESULTS$over_represented_pvalue),]
        
        # Size of each set
        GSET_LIST <- inverseList(GSET_LIST)
        tmp <- as.data.frame(sapply(GSET_LIST,length))
        RESULTS$sizeCat <- tmp[rownames(RESULTS),]
        
        # Overlapping gene list for each set    
        overlap <- function(x,EXP_MAT){
          L <- x[x %in% names(allGenes)[allGenes==TRUE]]
          return(c(length(L),paste(L,collapse=',')))      
        }
        tmp <- t(sapply(GSET_LIST,overlap,EXP_MAT))
        RESULTS$overlap_size <- tmp[rownames(RESULTS),1]
        RESULTS$overlap_symbols <- tmp[rownames(RESULTS),2]
        
        # Significant overlapping gene list for each set    
        overlap <- function(x,EXP_MAT){
          L <- x[x %in% names(allGenes)[allGenes==TRUE]]
          return(c(length(L),paste(L,collapse=',')))      
        }
        tmp <- t(sapply(GSET_LIST,overlap,EXP_MAT[allGenes == 1,]))
        RESULTS$sig_overlap_size <- tmp[rownames(RESULTS),1]
        RESULTS$sig_overlap_symbols <- tmp[rownames(RESULTS),2]
        
        RESULTS <- cbind(module,RESULTS)
        rownames(RESULTS) <- NULL
      }    
      ALL_RESULTS <- rbind(ALL_RESULTS,RESULTS[RESULTS$over_represented_pvalue<=PVAL,])
    }
    
    write.table(ALL_RESULTS,file=FILE_NAME,sep='\t',quote=F,row.names=F)
    
    return(list(FILE_NAME=FILE_NAME))
  }
  
  empty <- function() {return(FALSE)}
  
  name <- 'GOSeq'
  
  parameters <- function(){list(GENEIDS = GENEIDS, GENOMES = GENOMES, GO_CATEGORY = GO_CATEGORY, METHOD = METHOD)}
  
  return(list(calcEnrichment = calcEnrichment, empty=empty, name=name, parameters=parameters))
}


#############################################################################################################
GSVA <- function(
  ###########################################################################################################
  #### Methods Specific Options ####
  GSET_LIST = NULL, # if NULL uses MSigDB V4.0 from synapse 'syn2755203'
  METHOD = "gsva", # other options are c("ssgsea", "zscore", "plage")
  RNASEQ = FALSE, # since data is voom normalis
  DB = NULL,
  PARALLEL_SIZE = 14, # Number of parallel workers requested
  NUMBER_OF_BOOTSTRAP = 0  
){
  
  calcEnrichment <- function(COEXP_OBJ,COEXP_MAT,DE_OBJ,DE_MAT,EXP_MAT,DESIGN_MAT,RESULTS_DIR,PVAL){
    ###################################################################################################################
    # Enrichment Analysis by GOSeq accounting for length variations of genes
    ###################################################################################################################
    FILE_NAME <- paste(RESULTS_DIR,gsub('.tsv',paste(paste('',DB,METHOD,sep='_'),'.tsv',sep=''),COEXP_OBJ$properties$name),sep='/')    
    
    # Get MSigDB from synapse
    if (is.null(GSET_LIST)){
      OBJ <- synGet('syn2755203')      
      GSET_LIST <- dget(getFileLocation(OBJ))
    }
    
    # Attach DExp Gene List to the user supplied Genelist
    tmp <- list(rownames(EXP_MAT)[DE_MAT$adj.P.Val<=0.05])
    names(tmp) <- as.character(DE_OBJ$properties$name)
    GSET_LIST <- c(GSET_LIST, tmp)
    
    ALL_RESULTS = data.frame()
    for (module in levels(COEXP_MAT[,'Module'])){
      print(module)
      
      # Perform GSVA
      GSVA_RESULTS <- gsva(EXP_MAT[COEXP_MAT$Module==module,],
                           GSET_LIST,method=METHOD,rnaseq=RNASEQ,verbose=TRUE,parallel.sz=PARALLEL_SIZE)
      GSVA_RESULTS <- as.data.frame(GSVA_RESULTS$es.obs)
      
      # Fit linear model to GSVA observations
      FIT <- lmFit(GSVA_RESULTS, DESIGN_MAT)
      
      # Estimate moderated t-statistics
      FIT <- eBayes(FIT)
      
      # Differential expression for different sets
      RESULTS <- topTable(FIT, coef="Dx", number=length(GSET_LIST)) 
      if(dim(RESULTS)[1]==1)
        rownames(RESULTS) <- rownames(GSVA_RESULTS)
      
      # Size of each set
      tmp <- as.data.frame(sapply(GSET_LIST,length))
      RESULTS$set_size <- tmp[rownames(RESULTS),]
      
      # Overlapping gene list for each set
      overlap <- function(x,EXP_MAT){
        L <- x[x %in% rownames(EXP_MAT)]
        return(c(length(L),paste(L,collapse=',')))      
      }
      tmp <- t(sapply(GSET_LIST,overlap,EXP_MAT))
      RESULTS$overlap_size <- tmp[rownames(RESULTS),1]
      RESULTS$overlap_symbols <- tmp[rownames(RESULTS),2]
      
      # Significant Overlapping gene list for each set
      overlap <- function(x,EXP_MAT){
        L <- x[x %in% rownames(EXP_MAT)]
        return(c(length(L),paste(L,collapse=',')))      
      }
      tmp <- t(sapply(GSET_LIST,overlap,EXP_MAT[COEXP_MAT$Module==module,]))
      RESULTS$sig_overlap_size <- tmp[rownames(RESULTS),1]
      RESULTS$sig_overlap_symbols <- tmp[rownames(RESULTS),2]
      RESULTS <- cbind(rownames(RESULTS),RESULTS)
      colnames(RESULTS)[1] <- 'Category'
      RESULTS <- cbind(module,RESULTS)
      rownames(RESULTS) <- NULL
      
      ALL_RESULTS <- rbind(ALL_RESULTS,RESULTS[RESULTS$adj.P.Val<=PVAL,])
    }
    
    write.table(ALL_RESULTS,file=FILE_NAME,sep='\t',quote=F,row.names=F)
    
    return(list(FILE_NAME=FILE_NAME))
  }
  
  empty <- function() {return(FALSE)}
  
  name <- 'GSVA'
  
  parameters <- function(){list(DB = DB, METHOD = METHOD, RNASEQ = RNASEQ, NUMBER_OF_BOOTSTRAP = NUMBER_OF_BOOTSTRAP)}
  
  return(list(calcEnrichment = calcEnrichment, empty=empty, name=name, parameters=parameters))
}
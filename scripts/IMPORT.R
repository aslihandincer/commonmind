library(xhmmScripts)


ensureDir <- function(dirName) {
    if (!file.exists(dirName)) {
        dir.create(dirName, recursive=TRUE)
    }
}

my_warn_stdout <- function(msg) {
    writeLines(paste("WARNING: ", msg, sep=""))
}


SET_THIS_SCRIPT <- function() {
    if (sys.nframe() >= 1) {
        #
        #return(sys.frame(1)$ofile)
        #
        
        # Return the deepest frame with a file:
        for (i in sys.nframe():1) {
            fl = sys.frame(i)$ofile
            if (!is.null(fl)) {
                return(fl)
            }
        }
    }
    
    return(NULL)
}

# Needs to be called **DIRECTLY** from within the script function being started:
START_SCRIPT <- function(PARAMS, RUN_LOG.FILE, STORE_TO_SYNAPSE) {
    # Re-direct stdout to the log file, but still writing to stdout as well:
    sink(file=RUN_LOG.FILE, type="output", split=TRUE)

    writeLines(paste("\nSTART: ", Sys.time(), "\n", sep=""))
    
    writeLines("######################################################################")
    writeLines("# Function arguments")
    writeLines("######################################################################")
    for (i in 1:length(PARAMS)) {
        varName = names(PARAMS)[i]
        writeLines(paste("\n'", varName, "':", sep=""))

        # NOTE: We call eval() TWICE here since we manually quote some arguments, and then call do.call() with quote=TRUE:
        val = eval(eval(PARAMS[[i]], envir=parent.frame()), envir=parent.frame())
        if (is.null(val)) {val = "NULL"}
        if ((typeof(val) == "list" && !is.null(names(val))) || typeof(val) == "closure") {
            print(val)
        }
        else {
            writeLines(paste(val, sep=""))
        }
    }
    writeLines("######################################################################\n")

    THIS_EXECUTED = NULL
    if (STORE_TO_SYNAPSE) {
        THIS_EXECUTED = GET_THIS_EXECUTED(THIS_SCRIPT_PATH, warn=TRUE)
    }
    return(THIS_EXECUTED)
}

END_SCRIPT <- function(RUN_LOG.FILE, THIS_EXECUTED, ALL_USED_SYNAPSE_OBJECTS, STORE_SYNAPSE_FILES, ROOT_STORE_DIR_SYNAPSE_ID, SYNAPSE_SUB_DIR_NAME, STORE_TO_SYNAPSE, ACTIVITY_NAME) {
    writeLines(paste("\nDONE: ", Sys.time(), "\n", sep=""))

    # Set the run log to be uploaded last:
    COMMON_ANNOTATION_LABELS = unique(unlist(sapply(STORE_SYNAPSE_FILES, function(fInfo) {names(fInfo[-1])})))
    COMMON_ANNOTATIONS = sapply(COMMON_ANNOTATION_LABELS, function(lab) {vals = unique(sapply(STORE_SYNAPSE_FILES, function(fInfo) {fInfo[[lab]]})); if (length(vals) == 1) {vals}})
    COMMON_ANNOTATIONS = COMMON_ANNOTATIONS[!sapply(COMMON_ANNOTATIONS, is.null)]
    STORE_SYNAPSE_FILES[[length(STORE_SYNAPSE_FILES)+1]] = c(RUN_LOG.FILE, COMMON_ANNOTATIONS)

    SYNAPSE_SUB_DIR_ID = NULL
    if (STORE_TO_SYNAPSE) {
        writeLines("\nStoring all results files to Synapse.")
        
        SYNAPSE_SUB_DIR_ID = synStore(Folder(name=SYNAPSE_SUB_DIR_NAME, parentId=ROOT_STORE_DIR_SYNAPSE_ID))$properties$id
        writeLines(paste("\nCreated sub-directory '", SYNAPSE_SUB_DIR_NAME, "' [", SYNAPSE_SUB_DIR_ID, "] in folder ", ROOT_STORE_DIR_SYNAPSE_ID, sep=""))
        writeLines(paste("URL:\n", getSynapseURL(SYNAPSE_SUB_DIR_ID), sep=""))

        # Ensure that RUN_LOG.FILE is explicitly closed before appending to it:
        sink(file=NULL, type="output")
        # Keep re-directing stdout to the same log file (via append=TRUE), but WITHOUT also writing to stdout anymore (via split=FALSE):
        sink(file=RUN_LOG.FILE, append=TRUE, type="output", split=FALSE)

        EXECUTED_LIST = list(
            THIS_EXECUTED
            )
        
        for (storeInd in 1:length(STORE_SYNAPSE_FILES)) {
            # filePath should be the first element in the list:
            filePath = STORE_SYNAPSE_FILES[[storeInd]][[1]]
            # All other elements must be NAMED annotations:
            fileAnnots = STORE_SYNAPSE_FILES[[storeInd]][-1]
            
            writeLines(paste("\nUploading '", filePath, "' to Synapse in folder ", SYNAPSE_SUB_DIR_ID, sep=""))

            storeFile = File(path=filePath, parentId=SYNAPSE_SUB_DIR_ID)
            if (is.list(fileAnnots) && length(fileAnnots) > 0) {
                annotationValues(storeFile) = fileAnnots
            }

            MAX_ATTEMPTS = 10; MIN_INTER_ATTEMPT_SLEEP_TIME = 20; MAX_INTER_ATTEMPT_SLEEP_TIME = 300
            uploaded = FALSE; attempts = 0
            while (!uploaded && attempts < MAX_ATTEMPTS) {
                tryCatch({attempts = attempts + 1; if (attempts > 1) {writeLines(paste("Upload attempt # ", attempts, sep=""))}; storeFile = synStore(storeFile, used=ALL_USED_SYNAPSE_OBJECTS, executed=EXECUTED_LIST, activityName=ACTIVITY_NAME, forceVersion=FALSE); uploaded = TRUE}, error = function(cond) { if (attempts >= MAX_ATTEMPTS) {stop(paste("Unable to upload '", filePath, "'", sep=""))} else {sleepSec=round(runif(1, min=MIN_INTER_ATTEMPT_SLEEP_TIME, max=MAX_INTER_ATTEMPT_SLEEP_TIME)); writeLines(paste("Sleeping for ", sleepSec, " seconds.", sep="")); Sys.sleep(sleepSec)} })
            }
            
            writeLines(paste("Uploaded '", filePath, "' to Synapse: ", storeFile$properties$id, sep=""))
        }
    }
    
    sink(file=NULL, type="output")

    return(list(SYNAPSE_SUB_DIR_ID=SYNAPSE_SUB_DIR_ID, STORE_SYNAPSE_FILES=STORE_SYNAPSE_FILES))
}


saveData <- function(saveFile, saveNow) {
    if (saveNow) {
        saveVars = c()
        saveVars = union(saveVars, ls(all=TRUE, envir=globalenv()))
        saveVars = union(saveVars, ls(all=TRUE, envir=parent.env(environment())))
        saveVars = union(saveVars, ls(all=TRUE, envir=parent.frame()))

        save(list=saveVars, file=saveFile, envir=parent.frame())
    }
}



GET_THIS_EXECUTED <- function(THIS_SCRIPT_PATH, warn=FALSE) {
    if (is.null(THIS_SCRIPT_PATH) || !fileIsInGitRepo(THIS_SCRIPT_PATH) || filesGitCloneHasAnyDiffsFromRepo(THIS_SCRIPT_PATH)) {
        if (warn) {
            my_warn_stdout("Will not store provenance of executed script for output files to Synapse, since not all source code is committed to repository!")
            return(NULL)
        }
        stop("Cannot store output files to Synapse until all source code is committed to repository!")
    }

    THIS_EXECUTED = getCMCscriptURL(THIS_SCRIPT_PATH)
    writeLines(paste("\nExecuting script at: ", THIS_EXECUTED, sep=""))
    
    #THIS_EXECUTED = list(url=THIS_EXECUTED, name=basename(THIS_EXECUTED))

    return(THIS_EXECUTED)
}



getActualFileName <- function(fileName) {
    system(paste("readlink -f ", fileName, sep=""), intern=TRUE)
}



SYNAPSE_URL = 'https://www.synapse.org'


getSynapseURL <- function(synapseID) {
    paste(SYNAPSE_URL, "/", "#!Synapse:", synapseID, sep="")
}


getCMCscriptURL <- function(scriptName) {
    realScriptName = getActualFileName(scriptName)
    scriptPath = dirname(realScriptName)
    
    latestCommit = system(paste("cd ", scriptPath, " && git rev-parse HEAD", sep=""), intern=TRUE)
    scriptPrefix = system(paste("cd ", scriptPath, " && git rev-parse --show-prefix", sep=""), intern=TRUE)

    repoURL = system(paste("cd ", scriptPath, " && git config --get remote.origin.url", sep=""), intern=TRUE)
    repoURL = sub('^git@bitbucket\\.org:', 'https://bitbucket.org/', repoURL, perl=TRUE)
    repoURL = sub('\\.git$', '/src', repoURL, perl=TRUE)

    scriptURL = gsub("//", "/", paste(latestCommit, scriptPrefix, basename(scriptName), sep="/"))
    scriptURL = paste(repoURL, scriptURL, sep="/")
    return(scriptURL)
}

filesGitCloneHasAnyDiffsFromRepo <- function(scriptName) {
    realScriptName = getActualFileName(scriptName)
    scriptPath = dirname(realScriptName)
    
    hasDiffs = system(paste("cd ", scriptPath, " && git diff --no-ext-diff --quiet --exit-code", sep="")) != 0
    return(hasDiffs)
}

fileIsInGitRepo <- function(scriptName) {
    realScriptName = getActualFileName(scriptName)
    scriptPath = dirname(realScriptName)
    
    isRepo = system(paste("cd ", scriptPath, " && git status --short >& /dev/null", sep="")) == 0
    return(isRepo)
}



    
retrieveFromSynapse <- function(synapseID, dataName="data", maxDate=NULL, downloadFile=TRUE) {
    writeLines(paste("\nReading ", dataName, " from Synapse [", synapseID, "].", sep=""))
    
    getVersion = NULL
    if (!is.null(maxDate)) {
        writeLines(paste("Finding last version before date threshold: ", maxDate, sep=""))
        getVersionNumAndDate = getLatestSynapseVersionNumberUpToDate(synapseID, maxDate)
        getVersion = getVersionNumAndDate$versionNum
    }

    synapseObjectInfo = synGet(synapseID, version=getVersion, downloadFile=downloadFile)
    writeLines(paste("Downloaded version number ", synapseObjectInfo$properties$versionNumber, " of '", synapseID, "', modified on ", synapseObjectInfo$properties$modifiedOn, ".", sep=""))

    return(synapseObjectInfo)
}

getLatestSynapseVersionNumberUpToDate <- function(synapseID, maxDate=Sys.Date()) {
    synapseObjectInfo = synGet(synapseID, version=NULL, downloadFile=FALSE)
    versionsInfo = synapseObjectInfo$available.versions

    versionsDates = as.POSIXct(versionsInfo$modifiedOn)
    # Sort dates from most recent to most historical:
    sortedVersionsDates = sort.int(versionsDates, decreasing=TRUE, index.return=TRUE)
    earlyInds = which(sortedVersionsDates$x <= as.POSIXct(maxDate))
    if (length(earlyInds) == 0) {
        stop(paste("Cannot find any versions of '", synapseID, "' up to ", maxDate, sep=""))
    }
    versionIndex = sortedVersionsDates$ix[min(earlyInds)]

    versionNum = versionsInfo$versionNumber[versionIndex]
    versionDate = versionsInfo$modifiedOn[versionIndex]

    return(list(versionNum=versionNum, versionDate=versionDate))
}


getFileSynapseID <- function(parentID, name) {
    synapseQuery(paste('SELECT id, name FROM file WHERE parentId == "', parentID, '" AND name == "', name, '"', sep=""))[, "file.id"]
}

getAllFilesSynapseID <- function(parentID) {
    synapseQuery(paste('SELECT id, name FROM file WHERE parentId == "', parentID, '"', sep=""))
}


getFolderSynapseID <- function(parentID, name) {
    synapseQuery(paste('SELECT id, name FROM folder WHERE parentId == "', parentID, '" AND name == "', name, '"', sep=""))[, "folder.id"]
}

getAllFoldersSynapseID <- function(parentID) {
    synapseQuery(paste('SELECT id, name FROM folder WHERE parentId == "', parentID, '"', sep=""))
}


assignSynapseFileNames <- function(synapseFolderId, retrieveDataFiles, maxDate=NULL, filesPrefix=NULL, envir.get=parent.frame(), envir.assign=parent.frame()) {
    synapseFiles = getAllFilesSynapseID(synapseFolderId)
    if (!is.null(filesPrefix)) {
        keepInds = grep(paste("^", filesPrefix, sep=""), synapseFiles[, "file.name"])
        synapseFiles = synapseFiles[keepInds, ]
    }

    synapseObjects = list()
    for (dataName in retrieveDataFiles) {
        findSuffix = get(paste(dataName, ".SUFFIX", sep=""), envir=envir.get)
        findDataInds = grep(paste(findSuffix, "$", sep=""), synapseFiles[, "file.name"])
        if (length(findDataInds) != 1) {
            stop(paste("Unable to find unique file in '", synapseFolderId, "' with suffix '", findSuffix, "'", ifelse(!is.null(filesPrefix), paste(" [limited to files with prefix '", filesPrefix, "']", sep=""), ""), sep=""))
        }
        
        synapseId = synapseFiles[findDataInds[1], "file.id"]
        synapseObject = retrieveFromSynapse(synapseId, dataName=dataName, maxDate=maxDate)
        synapseObjects[[length(synapseObjects)+1]] = synapseObject

        dataNameFile = paste(dataName, ".FILE", sep="")
        assign(dataNameFile, getFileLocation(synapseObject), envir=envir.assign)
    }

    invisible(synapseObjects)
}



retrieveEnsemblToHGNCmap <- function(ENSEMBL_TO_HGNC_SYNAPSE_ID="syn2508851", maxDate=NULL) {
    mapperFileObject = retrieveFromSynapse(ENSEMBL_TO_HGNC_SYNAPSE_ID, dataName="ENSEMBL to HGNC mapper", maxDate=maxDate)
    mapperFile = getFileLocation(mapperFileObject)

    TABLE_mapper = read.table(mapperFile, sep="\t", header=TRUE, row.names=1, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE)

    mapper = as.character(TABLE_mapper[, 1])
    names(mapper) = rownames(TABLE_mapper)
    return(list(MAPPER=mapper, MAPPER_OBJECT=mapperFileObject))
}


insertMappedColumn <- function(dataTable, colNameToMap, MAPPER, newColName = paste0("MAPPED_", colNameToMap)) {
    if (!is.null(MAPPER)) {
        colNameToMapInd = which(colnames(dataTable) == colNameToMap)
        if (length(colNameToMapInd) == 1) {
            mappedGenes = as.character( MAPPER[ as.character(dataTable[, colNameToMap]) ] )
            beforeInds = 1:colNameToMapInd
            afterInds = c()
            if (colNameToMapInd < ncol(dataTable)) {
                afterInds = (colNameToMapInd + 1):ncol(dataTable)
            }
            dataTable = data.frame(dataTable[, beforeInds, drop=FALSE], mappedGenes, dataTable[, afterInds, drop=FALSE])
            colnames(dataTable)[colNameToMapInd + 1] = newColName
        }
    }
    
    return(dataTable)
}




PLINK_GENOTYPE_SUFFICES = c('fam', 'bim', 'bed')



mergeVectorsFromList <- function(lst) {
    unlist(sapply(unname(lst), identity))
}

# Will ensure that every category has a default value of 0:
dataToTable <- function(data, ensureNames=NULL) {
	data_table = table(data, useNA = "ifany")
	if (!is.null(ensureNames)) {
		tmp = c()
		tmp[ensureNames] = 0
		tmp[names(data_table)] = as.numeric(data_table)
		data_table = tmp
	}
	return(data_table)
}

# Taken from http://stackoverflow.com/questions/4452039/rs-equivalent-to-ind2sub-sub2ind-in-matlab :
ind2sub <- function(sz, ind) {
    ind = as.matrix(ind,ncol=1)
    sz = c(1,sz)
    den = 1
    sub = matrix(NA, nrow=length(ind), ncol=0)
    for(i in 2:length(sz)){
        den = den * sz[i-1]
        num = den * sz[i]
        s = floor(((ind-1) %% num)/den) + 1
        sub = cbind(sub,s)
    }
    return(sub)
}



getDataFile <- function(FILE_TYPE, CATEGORY) {
	DATA_FILES = "../data/data_files.txt"

	findFileCommand = paste("awk '{if ($1 == \"", FILE_TYPE, "\" && $2 == \"", CATEGORY, "\") print $3}' ", DATA_FILES, sep="")
	file = system(findFileCommand, intern=TRUE)
	if (length(file) == 0) {
		file = ""
	}

	return(file)
}

getSampleMetaData <- function() {
	SAMPLE_META_DATA_FILE = getDataFile("SAMPLE_META_DATA", "MASTER")
	SAMPLE_META_DATA = read.csv(SAMPLE_META_DATA_FILE, header=TRUE, check.names=FALSE, stringsAsFactors=TRUE, comment.char="", row.names=NULL, strip.white=TRUE)

	return(SAMPLE_META_DATA)
}


readCSVmetaDataFile <- function(META_DATA_CSV_FILE, colClasses=NA) {
    read.csv(META_DATA_CSV_FILE, header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE, colClasses=colClasses)
}

readMetaDataFile <- function(META_DATA_TSV_FILE, colClasses=NA) {
    read.table(META_DATA_TSV_FILE, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE, quote="", colClasses=colClasses)
}


readLimmaResults <- function(LIMMA_TSV_FILE) {
    read.table(LIMMA_TSV_FILE, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE, quote="")
}


removeIndividuals <- function(sampleMetaData, removeInds, removeGroupName, idColumn=NULL) {
    if (length(removeInds) != nrow(sampleMetaData)) {
        stop("Mismatch between: length(removeInds) != nrow(sampleMetaData)")
    }
    INDIV_ID_COLUMN = "Individual ID"
        
    if (length(which(removeInds)) > 0) {
        mainNames = rownames(sampleMetaData)[removeInds]
        if (!is.null(idColumn) && idColumn != INDIV_ID_COLUMN) {
            mainNames = sampleMetaData[removeInds, idColumn]
        }
        removeNames = paste(mainNames, " [", sampleMetaData[removeInds, INDIV_ID_COLUMN], "]", sep="")
        writeLines(paste("\nRemoving ", length(which(removeInds)), " ", removeGroupName, ": ", paste(removeNames, collapse=", "), sep=""))
        sampleMetaData = sampleMetaData[!removeInds, ]    
    }
    
    return(sampleMetaData)
}

removeDuplicatesBasedOnColumn <- function(idColumn, sampleMetaData, preferentiallyRemoveDuplicates=NULL, REMAINING_DUPLICATES.ACTION="REMOVE") {
    if (!is.null(preferentiallyRemoveDuplicates)) {
        # Remove duplicated individuals, as defined by preferentiallyRemoveDuplicates:
        duplicatedIndivs = names(which(table(sampleMetaData[, idColumn]) > 1))
        duplicatedInds = sampleMetaData[, idColumn] %in% duplicatedIndivs
        prioritizeForRemovalInds = preferentiallyRemoveDuplicates(sampleMetaData)
        removeInds = duplicatedInds & prioritizeForRemovalInds
        sampleMetaData = removeIndividuals(sampleMetaData, removeInds, "de-prioritized duplicated individuals", idColumn)
    }

    # Remove any remaining duplicated individuals (arbitrarily retaining the "first" one):
    removeInds = duplicated(sampleMetaData[, idColumn])
    numExtraRows = length(which(removeInds))
    if (numExtraRows > 0) {
        if (REMAINING_DUPLICATES.ACTION == "REMOVE") {
            sampleMetaData = removeIndividuals(sampleMetaData, removeInds, "still-duplicated individuals", idColumn)
        }
        else {
            remainingDupsMessage = paste("There are still ", numExtraRows, " duplicated rows remaining: ", paste(sampleMetaData[removeInds, idColumn], collapse=", "), sep="")
            if (REMAINING_DUPLICATES.ACTION == "LEAVE") {
                writeLines(paste("WARNING: ", remainingDupsMessage, sep=""))
            }
            else { # REMAINING_DUPLICATES.ACTION == "FAIL":
                stop(remainingDupsMessage)
            }
        }
    }

    return(sampleMetaData)
}




DEFAULT_RELEVELS = list("Dx"="Control", "Institution"="Pitt", "Ethnicity"="Caucasian", "Gender"="Female")

getDesignMatrix <- function(covariatesDataFrame, FACTOR_COVARIATE_NAMES, interactionTerms=NULL, RELEVELS=list()) {
    ROWNAMES = rownames(covariatesDataFrame)
    COLNAMES = colnames(covariatesDataFrame)
    FACTOR_COVARIATE_NAMES = setdiff(FACTOR_COVARIATE_NAMES, FACTOR_COVARIATE_NAMES[!(FACTOR_COVARIATE_NAMES %in% colnames(covariatesDataFrame))])
    NUMERIC_COVARIATE_NAMES = setdiff(COLNAMES, FACTOR_COVARIATE_NAMES)
    
    # Ensure the factors are in fact of type factor, and the quantitative variables are numeric:
    if (ncol(covariatesDataFrame) > 0) {
        covariatesDataFrame = as.data.frame( lapply(colnames(covariatesDataFrame), function(column) {if (column %in% FACTOR_COVARIATE_NAMES) {fac = as.factor(covariatesDataFrame[, column]); if (column %in% names(RELEVELS) && RELEVELS[[column]] %in% levels(fac)) {fac = relevel(fac, ref=RELEVELS[[column]])}; return(fac)} else {return(as.numeric(covariatesDataFrame[, column]))}}) )
        rownames(covariatesDataFrame) = ROWNAMES
        colnames(covariatesDataFrame) = COLNAMES
    }

    contra = NULL
    MAX_NUM_CATS = Inf
    catData = covariatesDataFrame[, FACTOR_COVARIATE_NAMES, drop=FALSE]
    if (ncol(catData) > 0) {
        numCats = sapply(colnames(catData), function(col) nlevels(factor(catData[, col])))
        EXCLUDE_CATEGORICAL_COLS = names(numCats)[numCats <= 1 | numCats > MAX_NUM_CATS]
        if (!is.null(EXCLUDE_CATEGORICAL_COLS) && length(EXCLUDE_CATEGORICAL_COLS) > 0) {
            warning(paste("Excluding categorical variables with less than 2", ifelse(is.infinite(MAX_NUM_CATS), "", paste(" or more than ", MAX_NUM_CATS, sep="")), " categories: ", paste(paste("'", EXCLUDE_CATEGORICAL_COLS, "'", sep=""), collapse=", "), sep=""))
            FACTOR_COVARIATE_NAMES = setdiff(FACTOR_COVARIATE_NAMES, EXCLUDE_CATEGORICAL_COLS)
            covariatesDataFrame = covariatesDataFrame[, !(colnames(covariatesDataFrame) %in% EXCLUDE_CATEGORICAL_COLS), drop=FALSE]
        }

        # Inspired by http://stackoverflow.com/questions/4560459/all-levels-of-a-factor-in-a-model-matrix-in-r
        #
        # And, already ensured above that covariatesDataFrame[, FACTOR_COVARIATE_NAMES] satisfies:
        # 1) fac is of type factor.
        # 2) fac is releveled as designated in RELEVELS.
        contra = lapply(FACTOR_COVARIATE_NAMES, function(column) {fac = covariatesDataFrame[, column]; return(contrasts(fac))})
        names(contra) = FACTOR_COVARIATE_NAMES
    }

    addInteractionFormula = ""
    if (!is.null(interactionTerms)) {
        interactionTermsStrings = sapply(interactionTerms, function(terms) {paste(terms, collapse=":")})
        missingTermsFromModel = sapply(interactionTerms, function(terms) {!all(terms %in% COLNAMES)})
        if (any(missingTermsFromModel)) {
            warning(paste("Dropping interaction terms for which one or more variables are not present in model: ", paste(paste("'", interactionTermsStrings[missingTermsFromModel], "'", sep=""), collapse=", "), sep=""))
            interactionTerms = interactionTerms[!missingTermsFromModel]
        }
        
        # Re-order based on ordering of variables (which determines how they will exist as named interactions [e.g., 'A:B' vs 'B:A']) :
        interactionTerms = sapply(interactionTerms, function(terms) {COLNAMES[COLNAMES %in% terms]}, simplify=FALSE)
        
        interactionTermsStrings = sapply(interactionTerms, function(terms) {paste(terms, collapse=":")})
        names(interactionTerms) = interactionTermsStrings
        
        if (length(interactionTermsStrings) > 0) {
            addInteractionFormula = paste(" + ", paste(interactionTermsStrings, collapse=" + "), sep="")
        }
        else {
            interactionTerms = NULL
        }
    }
    designFormula = as.formula(paste("~ .", addInteractionFormula, sep=""))

    # Inspired by http://stackoverflow.com/questions/5616210/model-matrix-with-na-action-null :
    current.na.action = getOption('na.action')
    # Model matrix will now include "NA":
    options(na.action='na.pass')

    # NOTE: this includes an '(Intercept)' column:
    if (ncol(covariatesDataFrame) > 0) {
        design = model.matrix(designFormula, data=covariatesDataFrame, contrasts.arg=contra)
    }
    else {
        design = model.matrix(as.formula("~ + 1"), data=covariatesDataFrame, contrasts.arg=contra)
    }
    rownames(design) = rownames(covariatesDataFrame)

    options(na.action=current.na.action)

    return(list(design=design, covariates=COLNAMES, interactionTerms=interactionTerms, factorsLevels=sapply(contra, colnames, simplify=FALSE), numericCovars=NUMERIC_COVARIATE_NAMES, covariatesDataFrame=covariatesDataFrame))
}

getCovariatesMap <- function(DESIGN_AND_COVARS) {
    FACTORS_AND_LEVELS = DESIGN_AND_COVARS$factorsLevels
    COVARIATES_MAP = sapply(1:length(FACTORS_AND_LEVELS), function(i) {paste(deparse(as.name(names(FACTORS_AND_LEVELS)[i]), backtick=TRUE), FACTORS_AND_LEVELS[[i]], sep="")}, simplify=FALSE)
    names(COVARIATES_MAP) = names(FACTORS_AND_LEVELS)

    numericCovars = DESIGN_AND_COVARS$numericCovars
    names(numericCovars) = numericCovars
    numericCovars = getSymbolicNamesList(numericCovars)
    COVARIATES_MAP = c(COVARIATES_MAP, numericCovars)

    interactionCovars = NULL
    if (!is.null(DESIGN_AND_COVARS$interactionTerms)) {
        interactionCovars = sapply(DESIGN_AND_COVARS$interactionTerms, function(terms) {allCombos=COVARIATES_MAP[terms]; numAllCombos=prod(sapply(allCombos, length)); combsMat=sapply(allCombos, function(comb) rep(comb, numAllCombos/length(comb))); sapply(1:nrow(combsMat), function(i) paste(combsMat[i, ], collapse=":"))}, simplify=FALSE)
    }
    COVARIATES_MAP = c(COVARIATES_MAP, interactionCovars)
    
    # Re-order as originally given:
    COVARIATES_MAP = COVARIATES_MAP[c(DESIGN_AND_COVARS$covariates, names(interactionCovars))]

    return(COVARIATES_MAP)
}

designMatVarsToCovars <- function(COVARIATES_MAP, designMatVars) {
    names(which(sapply(COVARIATES_MAP, function(v) any(v %in% designMatVars))))
}

getFullCovariates <- function(samplesByCovariates) {
    samplesByCovariates[, which(apply(samplesByCovariates, 2, function(dat) all(!is.na(dat))))]
}

getSymbolicNamesList <- function(strings) {
    lapply(strings, function(v) deparse(as.name(v), backtick=TRUE))
}

getSymbolicNamesVector <- function(strings) {
    unlist(getSymbolicNamesList(strings))
}







readRowsFromFile <- function(readFile, columns) {
	rowIDname = columns[1]
	tmpTable = read.table(readFile, sep="\t", header=FALSE, col.names=columns, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE)
	dupIDs = sort(names(which(table(tmpTable[, rowIDname]) > 1)))

	if (length(dupIDs) > 0) {
            my_warn_stdout(paste("Removing duplicate ", rowIDname, " IDs present in ", readFile, ": ", paste(paste('"', dupIDs, '"', sep=""), collapse=", "), sep=""))
	}
	removeRows = which(tmpTable[, rowIDname] %in% dupIDs)
	keepRows = setdiff(1:nrow(tmpTable), removeRows)

	retTable = tmpTable[keepRows, setdiff(colnames(tmpTable), rowIDname), drop=FALSE]
	rownames(retTable) = tmpTable[keepRows, rowIDname]
	return(retTable)
}

readGenesFromFile <- function(genesFile) {
	readRowsFromFile(genesFile, c("GENE", "LOCUS"))
}

readTranscriptsFromFile <- function(transcriptsFile) {
	readRowsFromFile(transcriptsFile, c("TRANSCRIPT", "GENE", "LOCUS"))
}

lociToChrBp1Bp2 <- function(loci) {
	CHR_BP1_BP2 = targetsToChrBp1Bp2(loci, startStopDelim="..")
	CHR_BP1_BP2 = data.frame(chr=CHR_BP1_BP2$chr, bp1=CHR_BP1_BP2$bp1, bp2=CHR_BP1_BP2$bp2, row.names=names(loci), check.names=FALSE, stringsAsFactors=FALSE)

	return(CHR_BP1_BP2)
}

intervalsOverlap <- function(start1, stop1, start2, stop2) {
    return (start1 <= stop2 & start2 <= stop1)
}





findOutliersInVector <- function(x) {
	MAX_SD = 3

	dist = MAX_SD * sd(x)
	outliers = which(x < mean(x) - dist | x > mean(x) + dist)
	outliers
}

findSignedOutliersInVector <- function(x) {
	outliers = findOutliersInVector(x)
	outliers = outliers[ which(sign(x[outliers]) != sign(mean(x))) ]
	outliers
}




formatMatrixPrecision <- function(matrixToFormat, decimalPrecision) {
    format(matrixToFormat, nsmall=decimalPrecision, digits=(decimalPrecision+1), drop0trailing=TRUE, trim=TRUE, justify='none')
}




readModulesFile <- function(modulesFile) {
    modulesData = read.table(modulesFile, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE)

    return(modulesData)
}

modulesDataToGeneAnnotations <- function(modulesData, geneCol="Gene") {
    return(convertTableToGeneAnnotations(modulesData, geneCol, "Module"))
}



retrieveGeneAnnotations <- function(GENE_ANNOTATION_SYNAPSE_ID="syn2749071", maxDate=NULL) {
    geneAnnotsSynapseObject = retrieveFromSynapse(GENE_ANNOTATION_SYNAPSE_ID, dataName="gene to annotation lists", maxDate=maxDate)
    geneAnnotsFile = getFileLocation(geneAnnotsSynapseObject)

    geneAnnots = read.table(geneAnnotsFile, sep="\t", header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, comment.char="", strip.white=TRUE)

    return(list(geneAnnotations=convertTableToGeneAnnotations(geneAnnots, "Gene", "Set"), geneAnnotationsSynapseObject=geneAnnotsSynapseObject))
}

convertTableToGeneAnnotations <- function(geneAnnotsTable, geneCol, annotCol, EXCLUDE_GENE_IDS=c(".")) {
    if (!is.null(EXCLUDE_GENE_IDS)) {
        keepRows = !(geneAnnotsTable[, geneCol] %in% EXCLUDE_GENE_IDS)
        geneAnnotsTable = geneAnnotsTable[keepRows, ]
    }

    geneAnnotsTable[, geneCol] = as.character(geneAnnotsTable[, geneCol])
    geneAnnotsTable[, annotCol] = as.character(geneAnnotsTable[, annotCol])

    genes = sort(unique(geneAnnotsTable[, geneCol]))
    annots = sort(unique(geneAnnotsTable[, annotCol]))

    annotToGenes = sapply(annots, function(a) geneAnnotsTable[geneAnnotsTable[, annotCol] == a, geneCol], simplify=FALSE)

    #
    #geneToAnnots = sapply(genes, function(g) sort(unique(geneAnnotsTable[geneAnnotsTable[, geneCol] == g, annotCol])), simplify=FALSE)
    #
    # Somewhat faster than above, since leverages the sparse structure (of most genes not being assigned to any particular annotation):
    # [Follows idea from: http://stackoverflow.com/questions/18538977/combine-merge-lists-by-elements-names ]
    getElementsIfNotExistEmpty <- function(lst, keys) {tmpLst=sapply(keys, function(x) vector(mode="character"), simplify=FALSE); tmpLst[names(lst)] = lst; return(tmpLst)}    
    l = lapply(names(annotToGenes), function(annot) {genes=annotToGenes[[annot]]; annots=rep(annot, length(genes)); names(annots)=genes; return(annots)})
    keys = sort(unique(unlist(lapply(l, names))))
    geneToAnnots = setNames(do.call(mapply, c(FUN=function(...) sort(unique(as.character(c(...)))), lapply(l, getElementsIfNotExistEmpty, keys), SIMPLIFY=FALSE)), keys)
    
    return(list(annotToGenes=annotToGenes, geneToAnnots=geneToAnnots))
}

subsetGeneAnnots <- function(geneAnnots, USE_GENES) {
    annotToGenes = geneAnnots$annotToGenes
    lst = lapply(names(annotToGenes), function(ann) {genes=annotToGenes[[ann]]; cbind(rep(ann, length(genes)), genes)})
    geneAnnotsTable = do.call(rbind, lst)
    colnames(geneAnnotsTable) = c("Annot", "Gene")

    geneAnnotsTable = geneAnnotsTable[geneAnnotsTable[, "Gene"] %in% USE_GENES, ]
    
    return(convertTableToGeneAnnotations(geneAnnotsTable, "Gene", "Annot"))
}


sqlite3Query <- function(query, dbFile, colClasses=NA, VERBOSE=FALSE) {
    if (VERBOSE) {
        writeLines(paste(Sys.time(), "\tStarting query:\n", query, " ;\n", sep=""))
    }

    SEP = "\t"
    sqlCmd = paste("echo \"", query, " ;\" | sqlite3 -header -list -separator '", SEP, "' -nullvalue NA ", dbFile, sep="")
    sqlCmdCon = pipe(sqlCmd)

    res = tryCatch({
        read.table(sqlCmdCon, sep=SEP, header=TRUE, row.names=NULL, check.names=FALSE, stringsAsFactors=FALSE, colClasses=colClasses)
    }, error = function(cond) { NULL })

    if (VERBOSE) {
        writeLines(paste(Sys.time(), "\tFinished query:\n", query, " ;\n\n", sep=""))
    }
    
    return(res)
}


sortedIndsByVal.NA_last <- function(vals, decreasing=TRUE) {
    IS_NA.vals = is.na(vals)
    sortedInds = c(which(!IS_NA.vals)[sort(vals, na.last=NA, decreasing=decreasing, index.return=TRUE)$ix], which(IS_NA.vals))
    return(sortedInds)
}


# Taken from: Examples section of help(toupper)
capwords <- function(s, strict = FALSE) {
    cap <- function(s) paste(toupper(substring(s, 1, 1)),
                             {s <- substring(s, 2); if(strict) tolower(s) else s},
                             sep = "", collapse = " " )
    sapply(strsplit(s, split = " "), cap, USE.NAMES = !is.null(names(s)))
}

formatNum <- function(num) {
    format(num, big.mark=",", scientific=FALSE)
}

extractVals <- function(LIST_OF_LISTS, key, defaultVal) {
    sapply(LIST_OF_LISTS, function(t) {val=defaultVal; if (key %in% names(t)) {val=t[[key]]}; return(val)})
}

makeDataFrame <- function(lst, colNames=NULL) {
    df = do.call(rbind, lapply(lst, data.frame, stringsAsFactors=FALSE))
    if (!is.null(df)) {
        colnames(df) = colNames
    }
    
    return(df)
}


binomTestOfProportions <- function(boolVec, alternative="two.sided") {
    passTable = table(boolVec, dnn=NULL)
    for (val in c("TRUE", "FALSE")) {
        if (!(val %in% names(passTable))) {
            passTable[val] = 0
        }
    }

    prop = NA
    p = 1
    Nstring = "0 / 0"
    if (sum(passTable) > 0) {
        prop = as.numeric(passTable["TRUE"]/sum(passTable))
        ## Since lower.tail=FALSE gives P[X > x], we need to plug in (x-1) to get P[X >= x]:
        #p = pbinom(passTable["TRUE"] - 1, size=sum(passTable), prob=0.5, lower.tail=FALSE)
        # Change to 2-sided test:
        p = binom.test(x=as.numeric(passTable["TRUE"]), n=sum(passTable), p=0.5, alternative=alternative)$p.value
        Nstring = paste(as.numeric(passTable["TRUE"]), "/", sum(passTable), sep="")
    }
    
    return(list(passTable=passTable, Nstring=Nstring, prop=prop, p=p))
}

pValuesToQQdata <- function(pVals) {
    sortP = sort(-log10(pVals), index.return=TRUE)
    y = sortP$x
    n = length(y)
    x = sort( -log10((1:n)/n) )

    return(list(x=x, y=y, resortInds=sortP$ix))
}


outlier_tolerant_row_zscores <- function(dat, OUTLIERS.NUM_SD=3){
  mns = apply(dat, 1, mean)
  sds = apply(dat, 1, sd)
  
  lower = mns - OUTLIERS.NUM_SD * sds
  upper = mns + OUTLIERS.NUM_SD * sds
  
  lower = matrix(rep(lower, ncol(dat)), ncol=ncol(dat), byrow=FALSE)
  upper = matrix(rep(upper, ncol(dat)), ncol=ncol(dat), byrow=FALSE)
  
  USE_INDS = !(dat < lower | dat > upper)
  MEAN_SD = t(sapply(1:nrow(dat), function(rInd) {
    inds = USE_INDS[rInd, ] 
    row=dat[rInd, inds]
    return(c("MEAN"=mean(row), "SD"=sd(row)))
  }))
  
  dat_zscores = t(sapply(1:nrow(dat), function(rInd) (dat[rInd, ] - MEAN_SD[rInd, "MEAN"]) / MEAN_SD[rInd, "SD"]))
  return(list(z=dat_zscores, outliers=!USE_INDS))
}


calcIntervalDistances <- function(INTERVALS1, INTERVALS2=INTERVALS1) {
    distMat = sapply(rownames(INTERVALS1), function(interv1) {i1 = INTERVALS1[interv1, ]; mat=matrix(sapply(rownames(INTERVALS2), function(interv2) {i2 = INTERVALS2[interv2, ]; if (i1[, "CHR"] != i2[, "CHR"]) {Inf} else {if (i1[, "BP2"] < i2[, "BP1"]) {i2[, "BP1"] - i1[, "BP2"]} else if (i2[, "BP2"] < i1[, "BP1"]) {i1[, "BP1"] - i2[, "BP2"]} else {0}}}, simplify=TRUE), nrow=1, byrow=TRUE); return(mat)}, simplify=TRUE)

    if (nrow(INTERVALS2) == 1) {
        distMat = t(t(distMat))
    }
    else {
        distMat = t(distMat)
    }
    
    rownames(distMat) = rownames(INTERVALS1)
    colnames(distMat) = rownames(INTERVALS2)
    
    return(distMat)
}





calcResiduals <- function(geneBySampleValues, samplesByCovariates, varsToAddBackIn=NULL, sampleWeights=NULL) {
    #################################################################################
    # "Recursively" use the calcResiduals() code after this section (in a for loop):
    #################################################################################
    if (is.matrix(sampleWeights)) {
        residualizedMat = matrix(NA, nrow=nrow(geneBySampleValues), ncol=ncol(geneBySampleValues), dimnames=dimnames(geneBySampleValues))
        for (gInd in 1:nrow(geneBySampleValues)) {
            gRow = calcResiduals(geneBySampleValues[gInd, , drop=FALSE], samplesByCovariates, varsToAddBackIn, sampleWeights[gInd, ])
            residualizedMat[gInd, ] = gRow
        }
        return(residualizedMat)
    }
    #################################################################################
        
    #result.lm = lsfit(x=samplesByCovariates, y=t(geneBySampleValues), wt=sampleWeights, intercept=FALSE)
        
    # Formula of "y ~ 0 + x" means no intercept:
    result.lm = lm(t(geneBySampleValues) ~ 0 + samplesByCovariates, weights=sampleWeights)
    covarNames = colnames(samplesByCovariates)
    
    coef = result.lm$coefficients
    isMatrixForm = is.matrix(coef)
    if (isMatrixForm) {
        rownames(coef) = covarNames
    }
    else {
        names(coef) = covarNames
    }

    allVarsToAddBack = '(Intercept)'
    allVarsToAddBack = c(allVarsToAddBack, varsToAddBackIn)
    allVarsToAddBack = intersect(allVarsToAddBack, covarNames)
    
    residualizedMat = result.lm$residuals
    if (isMatrixForm) {
        multCoef = coef[allVarsToAddBack, , drop=FALSE]
    }
    else {
        multCoef = coef[allVarsToAddBack]
    }
    residualizedMat = residualizedMat + samplesByCovariates[, allVarsToAddBack, drop=FALSE] %*% multCoef

    residualizedMat = t(residualizedMat)
    rownames(residualizedMat) = rownames(geneBySampleValues)
    colnames(residualizedMat) = colnames(geneBySampleValues)

    return(residualizedMat)
}
